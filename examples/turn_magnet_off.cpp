#include "ros/ros.h"
#include "mbzirc_mission/ActuateRelay.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "turn_magnet_off");
 
  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<mbzirc_mission::ActuateRelay>("/magnet_controller/actuate");
  mbzirc_mission::ActuateRelay srv;
  srv.request.on = false;
  
  if (client.call(srv))
  {
    ROS_INFO("Magnet is off!");      
  }
  else
  {
    ROS_ERROR("Failed to call service actuate!");
    return 1;
  }

  return 0;
}
