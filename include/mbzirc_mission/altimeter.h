#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <list>

struct LaserMeasure
{
    double Height;
    double Time;
};

class Altimeter
{
  bool landed_;
  bool readyToLand_;
  bool gotData_;
  double height_;
  std::vector<sensor_msgs::LaserScan> pastScans_;
  double landedH_;
  double readyToLandH_;
  bool isAboveDeck_;
  std::list<LaserMeasure> measures;
  double height_threshold_, time_range_;
  bool jumpDetectionStarted_;
  

public:
    Altimeter(ros::NodeHandle n)
    { 
        isAboveDeck_ = false;
        jumpDetectionStarted_ = false;
        height_threshold_ = 0;
        time_range_ = 0;

        landed_ = false;
        readyToLand_ = false;
        gotData_ = false;
        n.param("/mission_control/landed_height", landedH_, 0.1);
        n.param("/mission_control/ready_to_land_height", readyToLandH_, 0.5);	    
    }

    void LaserCB(const sensor_msgs::LaserScan::ConstPtr &msg)
    {
        int i;
        landed_=false;
        readyToLand_=false; 
        gotData_=true;

        if (msg->intensities[0]==1 && msg->ranges[0]>0.0)
        { // Only valid values are considered
            height_ = msg->ranges[0];
            if (pastScans_.size()<3)
            {
                pastScans_.push_back(*msg); 
            }
            else 
            { 
                pastScans_[0]=pastScans_[1];
                pastScans_[1]=pastScans_[2];
                pastScans_[2]=*msg;
                // ROS_INFO("Buffer %f %f %f ", pastScans_[0].ranges[0], pastScans_[1].ranges[0], pastScans_[2].ranges[0]);  

                for (i=0; i<3; i++)
                    if (pastScans_[i].ranges[0]>landedH_) 
                        break;
                if (i==3)
                {  // All pastScans are smaller than 0.1
                    landed_=true;
                    readyToLand_=true;
                    return;
                }

                for (i=0; i<3; i++)
                    if (pastScans_[i].ranges[0]>readyToLandH_) 
                        break;
                if (i==3) 
                {
                    readyToLand_=true;
                }
            } // else 
            
            if (jumpDetectionStarted_ == true)
            {
                int jumpResult = detectJump(height_threshold_, time_range_);
                if (jumpResult == -1)
                {
//                     if (isAboveDeck_ == false)
//                         ROS_ERROR("Deck detected");
                    isAboveDeck_ = true;
                }
                else if (jumpResult == +1)
                {
//                     if (isAboveDeck_ == true)
//                         ROS_ERROR("Ground detected");
                    isAboveDeck_ = false;
                }
            }
        }
    }

    bool isAboveDeck()
    {
        return isAboveDeck_;
    }
    
    bool isReadyToLand()
    {
        bool retvar = readyToLand_;
        readyToLand_= false;
        return retvar;
    }

    bool isLanded()
    {
        return landed_;
    }

    bool gotData()
    {
        return gotData_;
    }

    double height()
    {
        return height_;
    }

  // Detects a jump in the laser readings
  // Return values:
  // +1 -> Positive jump in readings (height increased)
  // 0	-> No jumps
  // -1 -> Negative jump in readings (height decreased)
  int detectJump(const double &height_threshold, const double &time_range)
  {
    // Don't process if haven't yet got data
    if (gotData_ == false) return 0;
          
    // Save parameters for the jump detection. Will be used later for detection if we are above the truck
    jumpDetectionStarted_ = true;
    height_threshold_ = height_threshold;
    time_range_ = time_range;
    
    // Wait for a drop in readings
    int type_of_jump = 0;

    LaserMeasure lm;
    lm.Height = height();
    lm.Time = ros::Time::now().toSec();
    measures.push_back(lm);

    // Remove old data
    while (!measures.empty() && lm.Time - measures.front().Time > time_range)
      measures.pop_front();

    // Find the maximum and minimum height in the data
    double max_height = 0; 
    double min_height = lm.Height;
    for (std::list<LaserMeasure>::iterator i = measures.begin(); i != measures.end(); ++i)
    {
      if (max_height < i->Height) 
	max_height = i->Height;
      if (min_height > i->Height) 
	min_height = i->Height;
    }

    // Remove unused data (data that came before the peak in the height)
    while (!measures.empty() && measures.front().Height < max_height && measures.front().Height > min_height)
	measures.pop_front();

    // Check if the difference with the new reading is larger than the threshold
    if (max_height - lm.Height > height_threshold)
      type_of_jump = -1;
    else if (lm.Height - min_height > height_threshold)
      type_of_jump = +1;
    
    return type_of_jump;
  }
};
