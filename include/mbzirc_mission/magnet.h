/*
* Copyright (c) 2016 Carnegie Mellon University, Guilherme Pereira <gpereira@cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include "ros/ros.h"
#include "mbzirc_mission/ActuateRelay.h"

#ifndef _MAGNET_CLASS

#define _MAGNET_CLASS

class Magnet
{
private:
  ros::ServiceClient client_;
  mbzirc_mission::ActuateRelay srv_;
public:
  Magnet(ros::NodeHandle& n){
       client_ = n.serviceClient<mbzirc_mission::ActuateRelay>("/magnet_controller/actuate"); 
  }
  
  bool turnOn(){
    srv_.request.on = true;
    if (client_.call(srv_))
    {
      return true; 
    }
    else
    {
      ROS_ERROR("Failed to call ActuateRelay (Magnet) service to turn on!");
      return false;
    }  
  }
  
  bool turnOff(){
    srv_.request.on = false;
    if (client_.call(srv_))
    {
      return true; 
    }
    else
    {
      ROS_ERROR("Failed to call ActuateRelay (Magnet) service to turn off!");
      return false;
    }  
  }
   
};

#endif
