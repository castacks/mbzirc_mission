#ifndef _MBZIRC_MISSION_MODES
#define _MBZIRC_MISSION_MODES

#define IDLE			(1 << 0)
#define GOTOWATCHPOS            (1 << 1)
#define GOTOTRUCK	        (1 << 2)
#define TRACK		        (1 << 3)
#define WATCHPOS		(1 << 4)
#define GIMBALTRACK 		(1 << 5)
#define LANDING 		(1 << 6)
#define GIMBALDOWN		(1 << 7)

#endif
