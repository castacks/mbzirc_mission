#include <ros/ros.h>
#include "mbzirc_mission/mission_modes.h"
#include <dji_sdk/dji_drone.h>
#include <ca_common/Trajectory.h>
#include "mbzirc_polytraj/jtraj.h"

#define MBZIRC_SIMULATION

namespace mbzirc 
{
namespace mission
{

enum class MissionMode : int {takeoff, gowatchpos, onwatchpos, movetotruck, decktrack, land, idle};

class Mission
{
private:
  ros::NodeHandle _handle;
  DJIDrone* _drone;
  CA::Vector3D _startpos;

  ca_common::Trajectory computeBestTrajectory(nav_msgs::Odometry currOdom, nav_msgs::Odometry goalOdom, 
      double tmin, double tmax, int nodeCount, double max_speed, double max_yawRate, double max_pitchAngle, long int seq)  
  {
    CA::jTraj traj;
	
    double tstep = 0.1;
    for (double t = tmin; t < tmax; t = t + tstep) 
    {
      // Plan the trajectory 
      traj.computeTrajectory(currOdom, goalOdom, t, nodeCount);

      // Check the feasibility of the trajectory 
      if (traj.respectConstraints(max_speed, max_yawRate, max_pitchAngle))
      {
	ROS_INFO("Trajectory respect constraints! t=%f", t);
	break;
      }
    }

    CA::Trajectory trajectory = traj.getTrajectory();

    ca_common::Trajectory trajectory_message = trajectory.toMsg();
    trajectory_message.header.stamp = ros::Time::now();
    trajectory_message.header.seq = seq++;
    trajectory_message.header.frame_id = "world";
    trajectory_message.cost=t; // The cost would be the time to reach the final destination.

    return trajectory_message;
  }

public:

  Mission(ros::NodeHandle& handle, CA::Vector3D startpos)
  {
    _handle = handle;
    _drone = new DJIDrone(handle);
    _startpos = startpos;
  }
  
  bool TakeOff()
  {
    _handle.setParam("/mission/mode", GOTOWATCHPOS);
    ros::spinOnce();
    ROS_INFO("Taking off!"); 

    #ifdef MBZIRC_SIMULATION 
      // This is for the simulated robot

      // Define the final pose
      CA::State final_state;
      final_state.pose.position_m = _startpos;
      final_state.pose.position_m[2] += 1.0;

      // Change the initial pose to allow take-off -> The simulator's constroller needs at least an abrupt movement of 1m
      currOdom.pose.pose.position.z += 0.5; // The trajectory starts above the current position

      // Compute the trajectory
      trajectory_message = computeBestTrajectory(currOdom, msgc(final_state), 1.0, 100, 2, max_speed, max_yawRate, max_pitchAngle, trajectory_message.header.seq);  

      ROS_INFO("Mission: Published trajectory - Take off!");
      trajectory_pub.publish(trajectory_message); 
    #else
      // This is for the real robot
      _drone->takeoff();
    #endif

    ros::Duration(4.5).sleep();
    return true;
  }
  
  bool GoToWatchingPosition()
  {
//     while (ros::ok())
//     {
//       n.setParam("/mission/mode", IDLE | GIMBALTRACK);
// 
//       using namespace DJI::onboardSDK;
//       uint8_t flag = Flight::HorizontalLogic::HORIZONTAL_VELOCITY | Flight::VerticalLogic::VERTICAL_VELOCITY 
// 	| Flight::YawLogic::YAW_PALSTANCE | Flight::HorizontalCoordinate::HORIZONTAL_GROUND | Flight::YawCoordinate::YAW_GROUND | Flight::SmoothMode::SMOOTH_ENABLE;
// 
//       n.getParamCached("/mission_control/zd", zd);
//       n.getParamCached("/mission_control/kpz", kpz);
// 
//       double z_error=zd-heightSense.height(); 
// 
//       if (heightSense.height()>(0.95*zd))
//       {
// 	mode=MissionMode::decktrack;
// 	n.setParam("/mission/mode", TRACK | GIMBALTRACK);	// this is here to speed-up switching to track mode.
// 	_drone->attitude_control(flag, 0, 0, kpz*z_error, 0);
//       }
//       else
//       {
// 	_drone->attitude_control(flag, 0, 0, kpz*z_error, 0);
// 	mode = MissionMode::gowatchpos;      
//       } 
//     }
    return true;
  }
  
  void Land()
  {
//     n.setParam("/mission/mode", LANDING);
// 
//     ROS_INFO("Landing!");  
// 
//     if (magnet.turnOn())
//       ROS_INFO("Magnet is on!");  
// 
//     _drone->landing();	    
// 
//     ros::Duration(5.0).sleep();
// 
//     if (heightSense.isLanded())
//       ROS_INFO("Landed!");
// 
//     if (magnet.turnOff())
//       ROS_INFO("Magnet is off!"); 
// 
//     ros::Duration(15.0).sleep();     
// 
//     mode=MissionMode::takeoff;
  }

  void TrackDeck()
  {
//     n.setParam("/mission/mode", TRACK | GIMBALTRACK);
//     mode=MissionMode::decktrack;
// 
//     if (heightSense.isReadyToLand() || heightSense.isLanded())
//       mode=MissionMode::land;
  }

  void Idle()
  {
//     n.setParam("/mission/mode", IDLE);
//     mode=MissionMode::idle; 
  }
  
};

}
}