#include "planB_modes.cpp"
//#define TIME_TO_WAIT 8.5F

void targetCallback(const opencv_apps::RotatedRectStamped::ConstPtr &msg)
{
   curr_rect=(*msg);
   time_got_rect = ros::Time::now();
   gotNewRectangle = true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "mission_control");
    ros::NodeHandle n;

    DJIDrone* drone;
    drone = new DJIDrone(n);

    ros::Publisher trajectory_pub = n.advertise<ca_common::Trajectory>("/mission_control/trajectory", 100);
    
    bool started = false;
    bool mission_time_started = false;
    double beginTime = ros::Time::now().toSec();
    double currentTime;

    // Read ROS parameters set in the launch file
    double loopRate, max_speed, max_speed_openLoop15, max_speed_openLoop10, max_speed_openLoop5, max_yawRate, max_pitchAngle, kpz, zd, ff15, ff10, ff5, moveDelay15, moveDelay10, moveDelay5, metersBelow, metersInFront, heightToApproach, time_to_accelerate, giveUpTimeLaser, giveUpTimeLanding;

    bool simulation;
    int nodeCount;
    
    ReadParams(n, loopRate, nodeCount, max_speed, max_speed_openLoop15, max_speed_openLoop10, max_speed_openLoop5, max_yawRate, max_pitchAngle, kpz, zd, ff15, ff10, ff5, moveDelay15, moveDelay10, moveDelay5, metersBelow, metersInFront, heightToApproach, simulation, onWaitForTruckPosHeight, time_to_accelerate, giveUpTimeLaser, giveUpTimeLanding);
    
    double max_speed_openLoop, ff, moveDelay;

    // Read the hard coded points of the track
    double p0x, p0y, p0z, p0h, p1x, p1y, p1z, p1h, p2x, p2y, p2z, p2h, p3x, p3y, p3z, p3h, p4x, p4y, p4z, p4h;
    ReadPoints(n, p0x, p0y, p0z, p0h, p1x, p1y, p1z, p1h, p2x, p2y, p2z, p2h, p3x, p3y, p3z, p3h, p4x, p4y, p4z, p4h);

    //changing the height of points to parameter
    p1z=onWaitForTruckPosHeight;
    p2z=onWaitForTruckPosHeight;
    p3z=onWaitForTruckPosHeight;
    p4z=onWaitForTruckPosHeight;

    n.param("/mission/authorized", started, false);

    ros::Subscriber sub4 = n.subscribe("/deck_track/target", 1, &targetCallback);
    time_got_rect = ros::Time::now();

    // Define the height laser and read data
    Altimeter heightSense(n);
    ros::Subscriber subLaser = n.subscribe("/sf30/range", 1 , &Altimeter::LaserCB, &heightSense);

    // Define the right and left lasers
    Altimeter laserRight(n);
    ros::Subscriber subLaserRight = n.subscribe("/sf30r/range", 1 , &Altimeter::LaserCB, &laserRight);
    
    Altimeter laserLeft(n);
    ros::Subscriber subLaserLeft = n.subscribe("/sf30l/range", 1 , &Altimeter::LaserCB, &laserLeft);

    // Test if all the lasers work! 
    if (ReadLaserData(heightSense) == false) return false;
    if (ReadLaserData(laserLeft) == false) return false;
    if (ReadLaserData(laserRight) == false) return false;
    
    ros::Subscriber subPose = n.subscribe("odom", 1, &getPose);
    ros::Subscriber image_sub = n.subscribe("/roaddetection/target", 1, &getPositionIndex);
    ros::Rate loop_rate(loopRate);

    ros::Duration(1.0).sleep();

    // Get transform with track position
    CA::Vector3D trackOrigin;
    double trackOrientation;
    tf::TransformListener listener;
    tf::StampedTransform transform;
    WaitForTransform(listener, transform, trackOrigin, trackOrientation);

    currentXYZ[0] = currentXYZ[1] = currentXYZ[2] = 0;

    CA::State state;
    ca_common::Trajectory trajectory_message;
    trajectory_message.header.seq=0;

    ros::Time time_saw_truck; 
    
    ros::Duration(2).sleep();

    CA::Vector3D deltaPose;
    //ROS_WARN("beginning: 1->3 world frame P3x=%f P3y=%f P3z=%f P1x=%f P1y=%f P1z=%f ",p3x,p3y,p3z,p1x,p1y,p1z);

    n.setParam("/mission/accelerate", false);

    n.setParam("/mission/mode", IDLE);

    MissionMode mode = MissionMode::takeoff;  // Controls the state machine
    
    bool StartKeyPressed = false;
    int HoverPoint = 0;
    
    while (ros::ok()) 
    {
        if (!StartKeyPressed)
            ROS_WARN_THROTTLE(1,"In memory of Azarakhsh, press the key to start the mission!!!");
        
        char ch = kbhit();
        switch (ch)
        {
            case RestartMissionKey:
                StartKeyPressed = false;
                mode = MissionMode::takeoff;
                break;
            case StartMissionKey:
                StartKeyPressed = true;
                HoverPoint = 0;
                if (mission_time_started == false)
                    beginTime = ros::Time::now().toSec();
                mission_time_started = true;
                break;
            case StartMissionP1Key:
                StartKeyPressed = true;
                HoverPoint = 1;
                if (mission_time_started == false)
                    beginTime = ros::Time::now().toSec();
                mission_time_started = true;
                break;
            case StartMissionP2Key:
                StartKeyPressed = true;
                HoverPoint = 2;
                if (mission_time_started == false)
                    beginTime = ros::Time::now().toSec();
                mission_time_started = true;
                break;
            case StartMissionP3Key:
                StartKeyPressed = true;
                HoverPoint = 3;
                if (mission_time_started == false)
                    beginTime = ros::Time::now().toSec();
                mission_time_started = true;
                break;
            case StartMissionP4Key:
                StartKeyPressed = true;
                HoverPoint = 4;
                if (mission_time_started == false)
                    beginTime = ros::Time::now().toSec();
                mission_time_started = true;
                break;
        }
                
        n.getParamCached("/mission/authorized", started);

        if (started && StartKeyPressed) 
        {
            switch ( mode ) 
            {
                case MissionMode::takeoff :  // Take off
                {
                    n.setParam("/mission/mode", GOTOWATCHPOS);
                    ros::spinOnce();

                    TakeOff(drone, simulation, state, trajectory_message, trajectory_pub, max_speed, max_yawRate, max_pitchAngle);
                    
                    using namespace DJI::onboardSDK;
                    uint8_t flag = Flight::HorizontalLogic::HORIZONTAL_VELOCITY | Flight::VerticalLogic::VERTICAL_VELOCITY 
                    | Flight::YawLogic::YAW_PALSTANCE | Flight::HorizontalCoordinate::HORIZONTAL_GROUND | Flight::YawCoordinate::YAW_GROUND;

                    drone->attitude_control(flag, 0, 0, 2.0F, 0);
                    ros::Duration(0.5F).sleep();
                    
                    drone->attitude_control(flag, 0, 0, 0, 0);
                    ros::Duration(0.1F).sleep();
                    
                    waitfortruckposition_index = 1; // for yamaha, hard code go to point 1
                    mode = MissionMode::gowaitfortruckpos; // for yamaha, hard code go to waitfortruckpos
                    
//                     if (HoverPoint == 0)
//                         mode = MissionMode::gowatchpos; //this is the actual plan B
//                     else 
//                     {
//                         waitfortruckposition_index = HoverPoint;
//                         mode = MissionMode::gowaitfortruckpos;
//                     }

                    // mode = MissionMode::gowaitfortruckpos; // this is just a hack to skip the first detection of the mission, COMMENT FOR REAL PLAN B
                    break;
                }
                case MissionMode::gowatchpos :  // Go to watch position
                {
                    n.setParam("/mission/mode",  GOTOWATCHPOS | GIMBALDOWN);
                    ros::spinOnce();

                    GoWatchPos(state, trajectory_message, trajectory_pub, nodeCount, max_speed, max_yawRate, max_pitchAngle, p0x, p0y, p0z, p0h);
                    
                    mode = MissionMode::onwatchpos;
                    gotPositionIndex = false;
                    break;
                }
                case MissionMode::onwatchpos : // Wait on watch pos until gets the deck direction
                {
                    n.setParam("/mission/mode", WATCHPOS | GIMBALDOWN);
                    ros::spinOnce();

                    OnWatchPos(n);
		    
                    
                    mode=MissionMode::gowaitfortruckpos;
                    break;
                }
                case MissionMode::gowaitfortruckpos :  // Go to watch position and wait for the truck
                {
                    n.setParam("/mission/mode",  GOTOWATCHPOS | GIMBALDOWN);
                    ros::spinOnce();
                    
                    GoWaitForTruckPos(state, trajectory_message, trajectory_pub, nodeCount, max_speed, max_yawRate, max_pitchAngle, p1x, p1y, p1z, p1h, p2x, p2y, p2z, p2h, p3x, p3y, p3z, p3h, p4x, p4y, p4z, p4h);
                    
                    // Next Mode
                    mode=MissionMode::onwaitfortruckpos;
                    ros::Duration(5.0).sleep(); // This is to prevent the drone to see the truck during the movement
                    break;
                }
                case MissionMode::onwaitfortruckpos : // Wait  until sees the deck
                {
                    n.setParam("/mission/mode", WATCHPOS | GIMBALDOWN);
                    ros::spinOnce();

                    int saw_truck = OnWaitForTruckPos(heightSense, laserRight, laserLeft, n, giveUpTimeLaser);
                   
                    if (saw_truck){
		         mode = MissionMode::movetotruck;
		         time_saw_truck = ros::Time::now();
		    }
		    else
		         mode = MissionMode::gowaitfortruckpos;
		    
                    break;
                }
                case MissionMode::movetotruck:  // Move to the oposite point passing over the truck
                {
                    n.setParam("/mission/mode",  GOTOTRUCK | GIMBALTRACK);
                    ros::spinOnce();

                    MoveToTruck(n, state, p1x, p1y, p1z, p2x, p2y, p2z, p3x, p3y, p3z, p4x, p4y, p4z, beginTime, currentTime, max_speed_openLoop, ff, moveDelay, max_speed_openLoop15, max_speed_openLoop5, ff15, ff5, moveDelay15, moveDelay5, deltaPose, max_yawRate, trajectory_message, nodeCount, trajectory_pub, time_saw_truck, giveUpTimeLanding);
                    
                    if ((ros::Time::now()-time_saw_truck).toSec() > giveUpTimeLanding) // Give up and go to the next point
                    {
                        ROS_WARN("Giving up and going to the next position!");
                        if (waitfortruckposition_index == 1)
                            waitfortruckposition_index = 1; //for yamaha, always go back to point 1
                        else if (waitfortruckposition_index == 2)
                            waitfortruckposition_index = 1;
                        else if (waitfortruckposition_index == 3)
                            waitfortruckposition_index = 4;
                        else if (waitfortruckposition_index == 4)
                            waitfortruckposition_index = 3;
                        
                        mode = MissionMode::gowaitfortruckpos;
                        continue;
                    }
//                     else
//                         ROS_WARN("Time passed: %0.2f seconds.", (ros::Time::now()-time_saw_truck).toSec());

                    mode = MissionMode::decktrack;
                    break;
                }
                case MissionMode::decktrack:  // deck tracking
                {
                    n.setParam("/mission/mode", TRACK | GIMBALTRACK);
                    ros::spinOnce();

                    // Accelerate the drone just after it sees the truck
                    ros::Duration time_since_saw_truck = ros::Time::now()-time_saw_truck; 
                    if (time_since_saw_truck.toSec()<time_to_accelerate) // Accelerate the drone
                        n.setParam("/mission/accelerate", true);
                    else
                        n.setParam("/mission/accelerate", false);
                
                
                    double decreaseHeightTime = 2.0F; // 2 seconds to decrease height before keeping the height
                    if ((ros::Time::now() - time_got_rect).toSec() < decreaseHeightTime)
                        n.setParam("/mission/decreaseHeight", true);
                    else
                        n.setParam("/mission/decreaseHeight", false);

		    
                    ROS_INFO("Mission: Deck Track");

                    if (heightSense.isReadyToLand() || heightSense.isLanded())
                        mode = MissionMode::land;
                    else if (heightSense.height() < heightToApproach)
                        mode = MissionMode::approachdeck;
                    else if (time_since_saw_truck.toSec() > giveUpTimeLanding) // Give up and go to the next point
                    {
                        ROS_WARN("Giving up and going to the next position!");
                        if (waitfortruckposition_index == 1)
                            waitfortruckposition_index = 1; //for yamaha, always go back to point 1
                        else if (waitfortruckposition_index == 2)
                            waitfortruckposition_index = 1;
                        else if (waitfortruckposition_index == 3)
                            waitfortruckposition_index = 4;
                        else if (waitfortruckposition_index == 4)
                            waitfortruckposition_index = 3;
                        
                        mode = MissionMode::gowaitfortruckpos;
                    }
//                      else
//                          ROS_WARN("Time passed: %0.2f seconds.", time_since_saw_truck.toSec());

                    break;
                }
                case MissionMode::approachdeck: // move towards the deck
                {
                    n.setParam("/mission/mode",  GOTOTRUCK | GIMBALTRACK);
                    ros::spinOnce();

                    ApproachDeck(state, trajectory_message, trajectory_pub, ff, metersInFront, metersBelow, max_yawRate, nodeCount);

                    mode = MissionMode::land;	 
                    break;
                }
                case MissionMode::land :  // landing
                {
                    n.setParam("/mission/mode", IDLE);  // This prevents the mux to send further commands to the robot
                    ros::spinOnce();

                    Land(heightSense, drone);

                    mode = MissionMode::idle;
                    break;
                }
                default:
                {
                    n.setParam("/mission/mode", IDLE);
                    mode = MissionMode::idle;
                    break;
                }
            } // end of switch

        } // end of if (started)
        else
        {
            n.setParam("/mission/mode", IDLE | GIMBALTRACK); // Do nothing except for tracking the target with the Gimbal
	    mode = MissionMode::takeoff;
        }

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}



