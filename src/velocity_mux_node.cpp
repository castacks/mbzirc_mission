#include <ros/ros.h>
#include <dji_sdk/dji_drone.h>
#include "mbzirc_mission/mission_modes.h"
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <tf/transform_listener.h>

class Velocity_mux{
  
  bool alreadyAuto_;
  DJIDrone* drone;
  ros::NodeHandle n_;
  ros::Publisher twist_command_pub_;
  long int seq_;
  int mode_;
  tf::TransformListener tf_listener_;
  bool simulation_;

public:
   Velocity_mux(ros::NodeHandle n): n_(n){
      drone = new DJIDrone(n_);
      twist_command_pub_ = n_.advertise<geometry_msgs::TwistStamped>("/command/twist", 1);
      seq_=0;
      n_.param("/mission_control/simulation", simulation_, false);
   }
  
  void trajectory(const geometry_msgs::TwistStamped &msg)
  {
   
    n_.getParamCached("/mission/mode", mode_);
    	    
    if (!(mode_ & TRACK) && !(mode_ & IDLE) ){
      
      
        // Send to DJI - trajectory velocities are computed in the world reference frame
	using namespace DJI::onboardSDK;
	uint8_t flag = Flight::HorizontalLogic::HORIZONTAL_VELOCITY | Flight::VerticalLogic::VERTICAL_VELOCITY 
		 | Flight::YawLogic::YAW_PALSTANCE | Flight::HorizontalCoordinate::HORIZONTAL_GROUND | Flight::YawCoordinate::YAW_GROUND;
	drone->attitude_control(flag, msg.twist.linear.x, msg.twist.linear.y, -msg.twist.linear.z, msg.twist.angular.z*180/M_PI);
      
	if (simulation_){
	  // Send to simulated quad
	  geometry_msgs::TwistStamped twist;
	  twist.header.frame_id = "/world";
	  twist.header.stamp = ros::Time::now();
	  twist.header.seq = seq_++;
	  twist.twist = msg.twist;
	  twist_command_pub_.publish(twist);
	}
      
    }
  }

  void visualServoing(const geometry_msgs::TwistStamped &msg)
  {  
    
    n_.getParamCached("/mission/mode", mode_);
		    
    if ((mode_ & TRACK) && !(mode_ & IDLE) ){
      
	  try{    
        
	    // Send to simulated quad
	    geometry_msgs::TwistStamped twist;
	    twist.header.frame_id = "/ned";
	    twist.header.stamp = ros::Time::now();
	    twist.header.seq = seq_++;
	
	    geometry_msgs::Vector3Stamped  velocity_vector_world, velocity_vector_body;
          
	    // Create a stamped vector to be transformed to the world 
	    velocity_vector_body.header.frame_id = "/base_frame"; 
	    velocity_vector_body.header.stamp = msg.header.stamp;
	
	    // Transform linear velocity
	    velocity_vector_body.vector = msg.twist.linear;
	    tf_listener_.waitForTransform("/base_frame", "/ned", velocity_vector_body.header.stamp, ros::Duration(0.2));
	    tf_listener_.transformVector("/ned", velocity_vector_body, velocity_vector_world);
	    twist.twist.linear = velocity_vector_world.vector;
	  	
	    // Transform angular velocity
	    velocity_vector_body.vector = msg.twist.angular;
	    tf_listener_.transformVector("/ned", velocity_vector_body, velocity_vector_world);
	    twist.twist.angular = velocity_vector_world.vector;
	    twist.twist.angular.x=twist.twist.angular.y=0.0;
	    
	    ROS_INFO("inside velocity MUX msg body Vx=%f Vy=%f Vz=%f ", msg.twist.linear.x, msg.twist.linear.y, msg.twist.linear.z);
        ROS_INFO("inside velocity MUX NED Vx=%f Vy=%f Vz=%f ", twist.twist.linear.x, twist.twist.linear.y, twist.twist.linear.z);
        
	    // Send to DJI 
	    using namespace DJI::onboardSDK;
	    uint8_t flag = Flight::HorizontalLogic::HORIZONTAL_VELOCITY | Flight::VerticalLogic::VERTICAL_VELOCITY 
		 | Flight::YawLogic::YAW_PALSTANCE | Flight::HorizontalCoordinate::HORIZONTAL_GROUND | Flight::YawCoordinate::YAW_GROUND;
	    drone->attitude_control(flag, twist.twist.linear.x, twist.twist.linear.y, -twist.twist.linear.z, twist.twist.angular.z*180/M_PI);
        
	    if (simulation_){
	    // Send to simulated quad
	    twist_command_pub_.publish(twist);
	    }
	     
	  }
	  catch (tf::TransformException ex){ // Cannot get transformations
	    ROS_WARN("velocity_mux: Cannot transform from body to world %s",ex.what());           
	  } // catch
	
    }
  }

  /*void visualServoing(const geometry_msgs::TwistStamped &msg)
  {  
    
    n_.getParamCached("/mission/mode", mode_);
		    
    if ((mode_ & TRACK) && !(mode_ & IDLE) ){
      
        // Send to DJI - visual servoing velocities are computed in the body reference frame
	using namespace DJI::onboardSDK;
	    uint8_t flag = Flight::HorizontalLogic::HORIZONTAL_VELOCITY | Flight::VerticalLogic::VERTICAL_VELOCITY 
		    | Flight::YawLogic::YAW_PALSTANCE | Flight::HorizontalCoordinate::HORIZONTAL_BODY | Flight::YawCoordinate::YAW_BODY;
	drone->attitude_control(flag, msg.twist.linear.x, msg.twist.linear.y, -msg.twist.linear.z, msg.twist.angular.z*180/M_PI);
	
	ROS_INFO("inside velocity MUX Vx=%f Vy=%f Vz=%f ", msg.twist.linear.x, msg.twist.linear.y, msg.twist.linear.z);
	
	if (simulation_){
	  try{    
        
	    // Send to simulated quad
	    geometry_msgs::TwistStamped twist;
	    twist.header.frame_id = "/world";
	    twist.header.stamp = ros::Time::now();
	    twist.header.seq = seq_++;
	
	    geometry_msgs::Vector3Stamped  velocity_vector_world, velocity_vector_body;
          
	    // Create a stamped vector to be transformed to the world 
	    velocity_vector_body.header.frame_id = "/base_frame"; 
	    velocity_vector_body.header.stamp = msg.header.stamp;
	
	    // Transform linear velocity
	    velocity_vector_body.vector = msg.twist.linear;
	    tf_listener_.waitForTransform("/base_frame", "/world", velocity_vector_body.header.stamp, ros::Duration(0.5));
	    tf_listener_.transformVector("/world", velocity_vector_body, velocity_vector_world);
	    twist.twist.linear = velocity_vector_world.vector;
	  	
	    // Transform angular velocity
	    velocity_vector_body.vector = msg.twist.angular;
	    tf_listener_.transformVector("/world", velocity_vector_body, velocity_vector_world);
	    twist.twist.angular = velocity_vector_world.vector;
	    twist.twist.angular.x=twist.twist.angular.y=0.0;
        
	    twist_command_pub_.publish(twist); 
	  }
	  catch (tf::TransformException ex){ // Cannot get transformations
	    ROS_WARN("velocity_mux: Cannot transform from body to world %s",ex.what());           
	  } // catch
	}
    }
  }*/
  
};

int main(int argc, char** argv){
  ros::init(argc, argv, "velocity_mux");
  ros::NodeHandle n;
  Velocity_mux VelocityMux(n);
  ros::Subscriber sub1 = n.subscribe("/velocity_mux/twist_trajectory", 1 , &Velocity_mux::trajectory, &VelocityMux);
  ros::Subscriber sub2 = n.subscribe("/velocity_mux/twist_visualservoing", 1 ,&Velocity_mux::visualServoing, &VelocityMux);

  ros::spin();
  return 0;
}
