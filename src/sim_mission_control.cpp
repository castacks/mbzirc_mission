#include "ros/ros.h"
#include <cstdlib>
#include <algorithm>
#include <ca_common/Trajectory.h>
#include <ca_common/math.h>
#include <ca_common/MissionWaypoint.h>
#include <ca_common/Mission.h>
#include <ca_common/MissionCommand.h>
#include <dji_sdk/dji_drone.h>
#include "mbzirc_prediction/GetPrediction.h"
#include "mbzirc_polytraj/jtraj.h"
#include "mbzirc_mission/mission_modes.h"
#include "mbzirc_mission/altimeter.h"
#include "mbzirc_mission/magnet.h"
#include "math_utils/math_utils.h"
#include <tf/transform_listener.h>
#include "mission.cpp"

bool gotPose = false;
CA::Vector3D currentXYZ;
double last_heading;
nav_msgs::Odometry currOdom;

void ReadParameters(ros::NodeHandle &n)
{
  
}

void getPose(const nav_msgs::OdometryConstPtr &msg)
{
  currOdom = (*msg);
  currentXYZ = CA::msgc((*msg).pose.pose.position);
  CA::QuatD orientation = CA::msgc((*msg).pose.pose.orientation);
  CA::Vector3D attitude = CA::quatToEuler(orientation);
  last_heading = attitude[2];
  gotPose = true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "mission_control"); 
    ros::NodeHandle n;
    ros::Publisher trajectory_pub = n.advertise<ca_common::Trajectory>("/mission_control/trajectory", 100);
  
    double loopRate;
    int nodeCount;
    double max_speed, max_yawRate, max_pitchAngle, kpz, zd;
    bool started = false;

    n.param("/mission_control/loopRate", loopRate, 20.0);
    n.param("/mission_control/nodeCount", nodeCount, 20);
    n.param("/mission_control/max_speed", max_speed, 1.0);
    n.param("/mission_control/max_yawRate", max_yawRate, 2.0);
    n.param("/mission_control/max_pitchAngle", max_pitchAngle, 0.61);
    
    n.param("/mission_control/kpz", kpz, 0.5);
    n.param("/mission_control/zd", zd, 2.0);
    
    n.param("/mission/authorized", started, false);

    Altimeter heightSense(n);

    ros::Subscriber subLaser = n.subscribe("/guidance/ultrasonic", 1 , &Altimeter::LaserCB, &heightSense);

    while (!heightSense.gotData() && ros::ok()){
	ros::spinOnce();
        ROS_INFO("Mission - Waiting for altimeter data...");
	ros::Duration(0.5).sleep();
    }
    if (!heightSense.gotData()){
        ROS_ERROR("Mission - Never got altimeter data!");
	return false;
    }
    else ROS_INFO("Mission - Got altimeter data...");

    ros::Subscriber subPose = n.subscribe("odom", 1, &getPose);
   
    ros::ServiceClient prediction_client = n.serviceClient<mbzirc_prediction::GetPrediction>("/prediction_node/prediction");	

    ros::Rate loop_rate(loopRate);
    
    Magnet magnet(n);

    magnet.turnOff();  

    ros::Duration(1.0).sleep(); 

    // Get transform with track position
    CA::Vector3D trackOrigin;
    double trackOrientation;
    tf::TransformListener listener;
    tf::StampedTransform transform;

    if (listener.waitForTransform("/world", "/track", ros::Time(0), ros::Duration(5.0)) ) {
       ROS_INFO("Mission - Using available transformation for track");
       listener.lookupTransform("/world", "/track", ros::Time(0), transform);
       trackOrigin[0] = transform.getOrigin().x();
       trackOrigin[1] = transform.getOrigin().y();
       trackOrigin[2] = 0.0;
       trackOrientation = transform.getRotation().getAngle();	
       ROS_INFO("Mission - Track Origin - X: %f Y: %f", trackOrigin[0], trackOrigin[1]);
    }
    else{
       ROS_WARN("Mission - No transformation received - Assuming track at (0,0)");
       trackOrigin*=0.0;
    }

    currentXYZ[0]=currentXYZ[1]=currentXYZ[2]=0;

    std::vector<CA::State, Eigen::aligned_allocator<CA::Vector3D> > state_list;
    ca_common::Trajectory trajectory_message;
    trajectory_message.header.seq=0;

      
    ros::Duration(2).sleep();	
    
    CA::Vector3D deltaPose;

    n.setParam("/mission/mode", IDLE); 
    
    mbzirc::mission::MissionMode mode = mbzirc::mission::MissionMode::takeoff;  // Controls the state machine

    mbzirc::mission::Mission mission = mbzirc::mission::Mission(n, currentXYZ);
    while (ros::ok()) {
	
	n.getParamCached("/mission/authorized", started);

	if (started) {
           
           switch ( mode ) {
	     case mbzirc::mission::MissionMode::takeoff :  // Take off 
	        {
/*		  
		  // Next Mode
		  mode=mbzirc::mission::MissionMode::gowatchpos;
		  */
  	       	  break;
	       	}
	    case mbzirc::mission::MissionMode::gowatchpos :  // Go to watch position
	        {
/*		  
		  n.setParam("/mission/mode",  GOTOWATCHPOS);
		  
		  ros::spinOnce();

		  // Define the final pose 
		  state.pose.position_m[0]=trackOrigin[0]+50*cos(trackOrientation); 
		  state.pose.position_m[1]=trackOrigin[1]+50*sin(trackOrientation);     
		  state.pose.position_m[2]= 16.0;
		  deltaPose[0]=trackOrigin[0]-state.pose.position_m[0];
		  deltaPose[1]=trackOrigin[1]-state.pose.position_m[1];  
		  state.pose.orientation_rad[2]=atan2(deltaPose[1], deltaPose[0]);	 
		
		  // Compute the trajectory
		  trajectory_message = computeBestTrajectory(currOdom, msgc(state), 1.0, 100, nodeCount, max_speed, max_yawRate, max_pitchAngle, trajectory_message.header.seq);  
	          
		  ROS_INFO("Mission: Published %5.2f seconds trajectory - Go to watch position", (double)trajectory_message.cost);
		  trajectory_pub.publish(trajectory_message);  	
	       	
		  // Wait the time of the trajectory
                  ros::Duration(trajectory_message.cost).sleep();
		  
		  // Next Mode
                  mode=mbzirc::mission::MissionMode::onwatchpos;
		*/
  	       	  break;
	       	}  
	    case mbzirc::mission::MissionMode::onwatchpos : // Wait on watch pos utit get enoght prediction data
		{
/*		  
		  n.setParam("/mission/mode", WATCHPOS );
		  
		  ROS_INFO("Mission: Waiting on Watch Position");
		  
		  int i=0;
		  while ((i++<2) && ros::ok()) {
		    
		    ros::spinOnce();          
		    ros::Duration(0.2).sleep();
		  }
		  
		  // Next Mode
                  mode=mbzirc::mission::MissionMode::movetotruck;*/

		  break;
		}     	
	    case mbzirc::mission::MissionMode::movetotruck : // Move to a future truck position at 3m from the ground
		{
/*		  
		  n.setParam("/mission/mode", GOTOTRUCK | GIMBALTRACK);
		  
		  ros::spinOnce();
  		   
		  mbzirc_prediction::GetPrediction srv;
		  CA::Trajectory trajectory;
 	          CA::jTraj traj;		
                  double tmin=1.0;
		  double tmax=150.0;
                  double tstep=0.1;
                  double t;
		  for (t=tmin;t<tmax;t=t+tstep) {  // Find the minimum time possible for landing

			srv.request.time=t;   // time for prediction
 
       		   	if (!prediction_client.call(srv))
  		    	{
    				ROS_ERROR("Failed to call service GetPrediction");
    				return 1;
  		    	}

		   	srv.response.pose_ahead.pose.pose.position.z = 4.0; // Change the height to hover the deck
			
			// Plan the trajectory to pose_ahead
			traj.computeTrajectory(currOdom, srv.response.pose_ahead, t, nodeCount);

			// Check the feasibility of the trajectory
			if (traj.respectConstraints(max_speed, max_yawRate, max_pitchAngle)){
			    //ROS_INFO("Mission: Trajectory respect constraints! t=%f",t);
			    break;
			}	

		   }	
                
		   trajectory=traj.getTrajectory();
               	   
                   trajectory_message = trajectory.toMsg();
               	   trajectory_message.header.stamp = ros::Time::now();
               	   trajectory_message.header.seq++;
               	   trajectory_message.header.frame_id = "world";
		   
	       	   ROS_INFO("Mission: Published %5.2f seconds trajectory - Move to Truck", t);
               	   trajectory_pub.publish(trajectory_message);
		   
                   ros::Duration(t).sleep(); 
		 
	       	   mode = mbzirc::mission::MissionMode::decktrack;
		   ROS_INFO("Mission: Changing to Visual Servoing");
		  */
		  break;
		}
		
	    case mbzirc::mission::MissionMode::decktrack:  // deck tracking
	       	{
  /*                	                   
		  n.setParam("/mission/mode", TRACK | GIMBALTRACK);   
		  		  
	       	  mode=mbzirc::mission::MissionMode::decktrack;
		  
		  if (heightSense.isReadyToLand() || heightSense.isLanded())
		      mode=mbzirc::mission::MissionMode::land;
		      */
		  break;
		}	
	    
            case mbzirc::mission::MissionMode::land :  // landing
	       	{
/*		  
		  n.setParam("/mission/mode", LANDING);
		  
		  ros::spinOnce();
		  
		  ROS_INFO("Landing!");  
		  
		  n.setParam("/mission/mode", IDLE);  // This prevents the mux to send further commands to the robot
		  ros::spinOnce();
		  
		  if (magnet.turnOn())
    			ROS_INFO("Magnet is on!");  
		  
		  drone->landing();
		  
		  
		   
                  ros::Duration(5.0).sleep(); 	

		  if (heightSense.isLanded())
		    	ROS_INFO("Landed!");

                  if (magnet.turnOff())
    			ROS_INFO("Magnet is off!"); 

		  ros::Duration(2.0).sleep();     
  		  
	       	  mode=mbzirc::mission::MissionMode::idle;
		  */
		  break;
		}
	  default:
		{
// 		  n.setParam("/mission/mode", IDLE);
// 		  
// 		  mode=mbzirc::mission::MissionMode::idle; 
// 		  
		  break;
		}
	   }
	 	
	} // if (started)
        else{

 	     n.setParam("/mission/mode", IDLE | GIMBALTRACK); // Do nothing except for tracking the target with the Gimbal 	
        }  
                 
	ros::spinOnce();
	loop_rate.sleep();
        
    }

    return 0;
}


