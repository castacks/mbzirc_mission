#include <ros/ros.h>
#include <dji_sdk/dji_drone.h>
#include <dji_sdk/FlightControlInfo.h>

class DJI_authorization{
  
  bool alreadyAuto;
  DJIDrone* drone;
  ros::NodeHandle n_;

public:
   DJI_authorization(ros::NodeHandle n): n_(n){
      drone = new DJIDrone(n_);
      alreadyAuto=false;
      n.setParam("/mission/authorized", false);
   }
  
  void getStartDji(const dji_sdk::RCChannels::ConstPtr &msg)
  {
      if(msg->mode > 4000.0)     
	  alreadyAuto = true;
      else 
	  alreadyAuto = false; 
  }

  void send_auth_req(const dji_sdk::FlightControlInfo::ConstPtr &msg)
  {  
      if (msg->serial_req_status == 1)
	  n_.setParam("/mission/authorized", true);
      else
	  n_.setParam("/mission/authorized", false);
    
      if (msg->serial_req_status!= 1 && alreadyAuto) // Two conditions to ask permission -> did not asked yet and remote in F
      {
	  drone->request_sdk_permission_control();
        
	  ROS_INFO("DJI Authorization node asked for permission to control de Drone!" );
	  ros::Duration(1.0).sleep(); 
      }
       
  }

};

int main(int argc, char** argv){
  ros::init(argc, argv, "dji_request_authorization");
  ros::NodeHandle n;
  DJI_authorization DJI_Drone(n);
  ros::Subscriber sub1 = n.subscribe("/dji_sdk/flight_control_info", 1 , &DJI_authorization::send_auth_req, &DJI_Drone);
  ros::Subscriber sub2 = n.subscribe("/dji_sdk/rc_channels", 1 ,&DJI_authorization::getStartDji, &DJI_Drone);

  ros::spin();
  return 0;
}
