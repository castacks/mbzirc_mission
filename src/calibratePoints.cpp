#include "ros/ros.h"
#include <cstdlib>
#include <algorithm>
#include <ca_common/Trajectory.h>
#include <ca_common/math.h>
#include <ca_common/MissionWaypoint.h>
#include <ca_common/Mission.h>
#include <ca_common/MissionCommand.h>
#include <dji_sdk/dji_drone.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>
#include <mbzirc_commons/commons.h>
#include <string>
#include <vector>
#include <fstream>

#define click_radius 10
#define window_name "Competition Field"
#define status_bar_height 30
#define right_panel_width 400
int status_bar_pos = 0;
int right_panel_pos = 0;
cv::Point mouse_pos = cv::Point(-1, -1);
int processing_point = -1;
int processed_data = 0;
cv::Vec4f gps_pos;
std::string launch_file = "";
std::string text_file = "";
bool isSaved = false;
double fixedHeight = 0.0F;

CA::Vector3D currentXYZ = CA::Vector3D(0, 0, 0);
double last_heading;
nav_msgs::Odometry currOdom;
bool gotPose = false;

class MyPoint
{
public:
    cv::Point image_pos; // (x, y)
    cv::Vec4d gps_pos;   // (x, y, z, h)
    bool isSet;          // Is the point already set?
    bool isInitialized;  // Is the point set before?
    
    void Set(bool is_set)
    {
        isSet = is_set;
        if (is_set) isInitialized = true;
    }
    
    MyPoint(int x, int y)
    {
        image_pos = cv::Point(x, y);
        isSet = false;
        isInitialized = false;
    }
    MyPoint()
    {
        image_pos = cv::Point(0, 0);
        isSet = false;
        isInitialized = false;
    }
};

std::vector<MyPoint> points(5);

void SetPointsAtStart()
{
    points[0] = MyPoint(280, 150);
    points[1] = MyPoint(195, 65);
    points[2] = MyPoint(375, 55);
    points[3] = MyPoint(375, 240);
    points[4] = MyPoint(195, 235);
}

void getPose(const nav_msgs::OdometryConstPtr &msg)
{
    currOdom = (*msg);
    currentXYZ = CA::msgc((*msg).pose.pose.position);
    CA::QuatD orientation = CA::msgc((*msg).pose.pose.orientation);
    CA::Vector3D attitude;
    attitude = CA::quatToEuler(orientation);
    last_heading = attitude[2];
    gotPose = true;
} 

void DrawPoint(cv::Mat &frame, MyPoint p, int index)
{
    cv::Scalar color, text_color;
    if (p.isSet)
    {
        color = cv::Scalar(0, 255, 0);
        text_color = cv::Scalar::all(0);
    }
    else if (p.isInitialized)
    {
        color = cv::Scalar(255, 0, 0);
        text_color = cv::Scalar::all(255);
    }
    else
    {
        color = cv::Scalar(0, 0, 255);
        text_color = cv::Scalar::all(255);
    }
    
    if (index == processing_point)
    {
        color = cv::Scalar(0, 255, 255);
        text_color = cv::Scalar::all(0);
    }
    
    cv::circle(frame, p.image_pos, click_radius, color, -1, 8);

    char point_num_text[4];
    sprintf(point_num_text, "%d", index);
    mbzirc::commons::OpenCVImageTools::PrintTextOnImage(frame, point_num_text, p.image_pos + cv::Point(-4, 4), text_color, 0.4, 1, cv::FONT_HERSHEY_DUPLEX);
}

void ShowImage(cv::Mat &img, cv::Mat &logo)
{
    // Create the new frame
    cv::Mat frame(img.rows + status_bar_height, img.cols + right_panel_width, CV_8UC3, cv::Scalar(0,0,0));
    img.copyTo(frame(cv::Rect(0, 0, img.cols, img.rows)));
    logo.copyTo(frame(cv::Rect(img.cols + (frame.cols - img.cols - logo.cols) / 2, img.rows - logo.rows, logo.cols, logo.rows)));
    
    // Write the current GPS data
    char status_text[200];
    sprintf(status_text, "GPS: (x = %0.3lf, y = %0.3lf, z = %0.3lf, h = %0.3lf)", currentXYZ[0], currentXYZ[1], currentXYZ[2], last_heading);
    mbzirc::commons::OpenCVImageTools::PrintTextOnImage(frame, status_text, cv::Point(10, status_bar_pos + 20), cv::Scalar(0, 255, 255), 0.4, 1, cv::FONT_HERSHEY_TRIPLEX);

//     // Add the current mouse position
//     sprintf(status_text, "Mouse: (%d, %d)", mouse_pos.x, mouse_pos.y);
//     mbzirc::commons::OpenCVImageTools::PrintTextOnImage(frame, mouse_pos_text, cv::Point(10, status_bar_pos + 20), cv::Scalar(0, 255, 255), 0.4, 1, cv::FONT_HERSHEY_TRIPLEX);
    
    // Write points data and draw the points
    int text_vert_pos = 0;
    for (int i = 0; i < points.size(); ++i)
    {
        DrawPoint(frame, points[i], i);
        
        if (points[i].isInitialized == false) continue;
        
        text_vert_pos += 20;
        char point_text[100];
        sprintf(point_text, "Point %d: (%0.3lf, %0.3lf, %0.3lf, %0.3lf)", i, points[i].gps_pos[0], points[i].gps_pos[1], points[i].gps_pos[2], points[i].gps_pos[3]);
        mbzirc::commons::OpenCVImageTools::PrintTextOnImage(frame, point_text, cv::Point(right_panel_pos + 10, text_vert_pos), cv::Scalar(0, 255, 255), 0.4, 1, cv::FONT_HERSHEY_TRIPLEX);
    }
    
    // Write if the data file is saved
    char save_text[50];
    cv::Scalar saved_text_color;
    if (isSaved)
    {
        sprintf(save_text, "The changes are saved to the file!");
        saved_text_color = cv::Scalar(100, 200, 0);
        mbzirc::commons::OpenCVImageTools::PrintTextOnImage(frame, save_text, cv::Point(right_panel_pos + 10, text_vert_pos + 50), saved_text_color, 0.4, 1, cv::FONT_HERSHEY_TRIPLEX);
    }
    
    cv::imshow(window_name, frame);
    cv::waitKey(1);
}

int DetectClickedPoint(int x, int y)
{
    for (int i = 0; i < points.size(); ++i)
        if (cv::norm(points[i].image_pos - cv::Point(x, y)) <= click_radius)
            return i;
        
    return -1;
}

void SaveData()
{
    std::ofstream launch_file_stream(launch_file);
    if (launch_file_stream.good())
    {
        launch_file_stream << "<?xml version=\"1.0\"?>" << std::endl;
        launch_file_stream << "<launch>" << std::endl;
        launch_file_stream << "\t<!-- Z direction is negative (NED Frame) and heading is in radians -->" << std::endl;
        launch_file_stream << "\t" << std::endl;
        
        for (int i = 0; i < points.size(); ++i)
        {
            launch_file_stream << "\t<!-- Data for point " << i << " -->" << std::endl;
            launch_file_stream << "\t<param name=\"p" << i << "x\" type=\"double\" value=\"" << points[i].gps_pos[0] << "\" />" << std::endl;
            launch_file_stream << "\t<param name=\"p" << i << "y\" type=\"double\" value=\"" << points[i].gps_pos[1] << "\" />" << std::endl;
            launch_file_stream << "\t<param name=\"p" << i << "z\" type=\"double\" value=\"" << points[i].gps_pos[2] << "\" />" << std::endl;
            launch_file_stream << "\t<param name=\"p" << i << "h\" type=\"double\" value=\"" << points[i].gps_pos[3] << "\" />" << std::endl;
            launch_file_stream << std::endl;
        }
        launch_file_stream << "</launch>" << std::endl;
    }

    std::ofstream text_file_stream(text_file);
    if (text_file_stream.good())
    {
        for (int i = 0; i < points.size(); ++i)
            text_file_stream << points[i].gps_pos[0] << " " << points[i].gps_pos[1] << " " << points[i].gps_pos[2] << " " << points[i].gps_pos[3] << std::endl;
    }
}

void MouseEventHandler(int event, int x, int y, int flags, void* userdata)
{
    if (event == cv::EVENT_RBUTTONDOWN)
    {
        int clicked_point = DetectClickedPoint(x, y);
        if (clicked_point == -1) return;
        points[clicked_point].Set(!points[clicked_point].isSet);
        
        // Check if all the points are done
        isSaved = false;
        for (int i = 0; i < points.size(); ++i)
            if (points[i].isSet == false)
                return;

        // Save the data
        SaveData();
        isSaved = true;
    }
    else if (event == cv::EVENT_LBUTTONDOWN)
    {
        int clicked_point = DetectClickedPoint(x, y);
        if (clicked_point == -1) return;
        
        isSaved = false;
        if (clicked_point == processing_point)
            processing_point = -1;
        else if (processing_point == -1)
            processing_point = clicked_point;
    }
    else if (event == cv::EVENT_MOUSEMOVE)
        mouse_pos = cv::Point(x, y);
}

void ProcessData(int numOfPointsToAverage)
{
    if (processing_point == -1) 
    {
        gps_pos = cv::Vec4d(0, 0, 0, 0);
        processed_data = 0;
        return;
    }
    
    processed_data++;
    gps_pos += cv::Vec4d(currentXYZ[0], currentXYZ[1], currentXYZ[2], last_heading);
    
    // Check if we are done processing
    if (processed_data < numOfPointsToAverage) return;
    
    // Average the GPS data
    points[processing_point].Set(true);
    points[processing_point].gps_pos = gps_pos / numOfPointsToAverage;
    
    // Check if fixed height to be enforced
    if (fixedHeight != 0)
        points[processing_point].gps_pos[2] = fixedHeight;
    
    // Reset
    gps_pos = cv::Vec4d(0, 0, 0, 0);
    processed_data = 0;
    processing_point = -1;
    
    // Check if all the points are done
    isSaved = false;
    for (int i = 0; i < points.size(); ++i)
        if (points[i].isSet == false)
            return;
    
    // Save the points to the files
    SaveData();
    isSaved = true;
}

void ReadData()
{
    std::ifstream text_file_stream(text_file);
    if (text_file_stream.good())
    {
        for (int i = 0; i < points.size(); ++i)
        {
            text_file_stream >> points[i].gps_pos[0] >> points[i].gps_pos[1] >> points[i].gps_pos[2] >> points[i].gps_pos[3];
            points[i].isInitialized = true;
        }
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "mission_control"); 

    ros::NodeHandle n;
    
    DJIDrone* drone;
    
    drone = new DJIDrone(n); 
  
    // Read the loop rate parameter
    double loopRate;
    n.param<double>("/calibrate_points/loopRate", loopRate, 20.0);
    
    // Read the fixed height parameter. Will ignore fixed height if it is zero
    n.param<double>("/calibrate_points/fixedHeight", fixedHeight, 0.0F);

    // Read the number of points needed for averaging
    int numOfPointsToAverage;
    n.param<int>("/calibrate_points/numOfPointsToAverage", numOfPointsToAverage, 50);

    // Read the path for the picture of the field
    std::string imagePath;
    n.param<std::string>("/calibrate_points/fieldImage", imagePath, "");
    cv::Mat img_field = cv::imread(imagePath, CV_LOAD_IMAGE_COLOR);
    
    // Read the path for the picture of the logo
    n.param<std::string>("/calibrate_points/logoImage", imagePath, "");
    cv::Mat img_logo = cv::imread(imagePath, CV_LOAD_IMAGE_COLOR);

    // Read the path for the launch and text data files
    n.param<std::string>("/calibrate_points/pointsLaunchFile", launch_file, "");
    n.param<std::string>("/calibrate_points/pointsTextFile", text_file, "");
    
    // Subscribe to odometry topic
    ros::Subscriber subPose = n.subscribe("odom", 1, &getPose);
   
    // Set the rate of the loop
    ros::Rate loop_rate(loopRate);
    ros::Duration(1.0).sleep(); 
   
    CA::State state;	 
    ros::Duration(2).sleep();	
    
    // Define the new window and set the callback function for mouse events
    cv::namedWindow(window_name, 1);
    cv::setMouseCallback(window_name, MouseEventHandler, NULL);
    status_bar_pos = img_field.rows;
    right_panel_pos = img_field.cols;

    // Set points on the image_pos
    SetPointsAtStart();
    
    // Read the previous data points
    ReadData();
    
    while (ros::ok())
    {
        ROS_INFO("(x y z h) %11.8f %11.8f %11.8f %11.8f ", currentXYZ[0], currentXYZ[1], currentXYZ[2], last_heading);
        ShowImage(img_field, img_logo);
        ProcessData(numOfPointsToAverage);
        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}


