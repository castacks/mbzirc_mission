#include "ros/ros.h"
#include <cstdlib>
#include <algorithm>
#include <ca_common/Trajectory.h>
#include <ca_common/math.h>
#include <ca_common/MissionWaypoint.h>
#include <ca_common/Mission.h>
#include <ca_common/MissionCommand.h>
#include <dji_sdk/dji_drone.h>
#include "mbzirc_prediction/GetPrediction.h"
#include "mbzirc_polytraj/jtraj.h"
#include "mbzirc_mission/mission_modes.h"
#include "mbzirc_mission/altimeter.h"
#include "mbzirc_mission/magnet.h"
#include "math_utils/math_utils.h"
#include <tf/transform_listener.h>


CA::Vector3D currentXYZ;
double last_heading;
nav_msgs::Odometry currOdom;
bool gotPose = false;


void getPose(const nav_msgs::OdometryConstPtr &msg)
{
    currOdom = (*msg);
    currentXYZ = CA::msgc((*msg).pose.pose.position);
    CA::QuatD orientation = CA::msgc((*msg).pose.pose.orientation);
    CA::Vector3D attitude;
    attitude = CA::quatToEuler(orientation);
    last_heading = attitude[2];
    gotPose = true;
} 

ca_common::Trajectory computeBestTrajectory(nav_msgs::Odometry currOdom, nav_msgs::Odometry goalOdom, double tmin, double tmax, int nodeCount, double max_speed, double max_yawRate, double max_pitchAngle, long int seq)  
{
 
    CA::jTraj traj;
    ca_common::Trajectory trajectory_message;
    CA::Trajectory trajectory;
       
    double tstep=0.1;
    double t;
    for (t=tmin;t<tmax;t=t+tstep) { 
        // Plan the trajectory 
        traj.computeTrajectory(currOdom, goalOdom, t, nodeCount);
                        
        // Check the feasibility of the trajectory 
  if (traj.respectConstraints(max_speed, max_yawRate, max_pitchAngle)){
      ROS_INFO("Mission: Trajectory respect constraints! t=%f",t);
      break;
  }
    }

    trajectory=traj.getTrajectory();

    trajectory_message = trajectory.toMsg();
    trajectory_message.header.stamp = ros::Time::now();
    trajectory_message.header.seq=seq++;
    trajectory_message.header.frame_id = "world";
    trajectory_message.cost=t; // The cost would be the time to reach the final destination.
    
    return trajectory_message;
  
}




enum class MissionMode : int{takeoff, gowatchpos, land, decktrack, idle};

int main(int argc, char **argv)
{
    ros::init(argc, argv, "mission_control"); 

    ros::NodeHandle n;
    
    DJIDrone* drone;
    
    drone = new DJIDrone(n); 

    ros::Publisher trajectory_pub = n.advertise<ca_common::Trajectory>("/mission_control/trajectory", 100);
  
    double loopRate;
    int nodeCount;
    double max_speed, max_yawRate, max_pitchAngle, kpz, zd;
    bool started = false;

    n.param("/mission_control/loopRate", loopRate, 20.0);
    n.param("/mission_control/nodeCount", nodeCount, 20);
    n.param("/mission_control/max_speed", max_speed, 1.0);
    n.param("/mission_control/max_yawRate", max_yawRate, 2.0);
    n.param("/mission_control/max_pitchAngle", max_pitchAngle, 0.61);
    
    n.param("/mission_control/kpz", kpz, 0.5);
    n.param("/mission_control/zd", zd, 2.0);
    
    n.param("/mission/authorized", started, false);

    Altimeter heightSense(n);

    ros::Subscriber subLaser = n.subscribe("/guidance/ultrasonic", 1 , &Altimeter::LaserCB, &heightSense);

    while (!heightSense.gotData() && ros::ok()){
  ros::spinOnce();
        ROS_INFO("Mission - Waiting for altimeter data...");
  ros::Duration(0.5).sleep();
    }
    if (!heightSense.gotData()){
        ROS_ERROR("Mission - Never got altimeter data!");
  return false;
    }
    else ROS_INFO("Mission - Got altimeter data..."); 

    ros::Subscriber subPose = n.subscribe("odom", 1, &getPose);
   
    ros::ServiceClient prediction_client = n.serviceClient<mbzirc_prediction::GetPrediction>("/prediction_node/prediction");  

    ros::Rate loop_rate(loopRate);
    
    Magnet magnet(n);

    magnet.turnOff();  

    ros::Duration(1.0).sleep(); 

    // Get transform with track position
    CA::Vector3D trackOrigin;
    double trackOrientation;
    tf::TransformListener listener;
    tf::StampedTransform transform;

    if (listener.waitForTransform("/world", "/track", ros::Time(0), ros::Duration(5.0)) ) {
       ROS_INFO("Mission - Using available transformation for track");
       listener.lookupTransform("/world", "/track", ros::Time(0), transform);
       trackOrigin[0] = transform.getOrigin().x();
       trackOrigin[1] = transform.getOrigin().y();
       trackOrigin[2] = 0.0;
       trackOrientation = transform.getRotation().getAngle(); 
       ROS_INFO("Mission - Track Origin - X: %f Y: %f", trackOrigin[0], trackOrigin[1]);
    }
    else{
       ROS_WARN("Mission - No transformation received - Assuming track at (0,0)");
       trackOrigin*=0.0;
    }

    currentXYZ[0]=currentXYZ[1]=currentXYZ[2]=0;

    std::vector<CA::State, Eigen::aligned_allocator<CA::Vector3D> > state_list;
    CA::State state;   
    CA::Trajectory trajectory;
    ca_common::Trajectory trajectory_message;
    trajectory_message.header.seq=1;

      
    ros::Duration(2).sleep(); 
    
    double heading;
    CA::Vector3D deltaPose;
    nav_msgs::Odometry takeoffPose;

    n.setParam("/mission/mode", IDLE); 
    
    MissionMode mode = MissionMode::takeoff;  // Controls the state machine

    while (ros::ok()) {
  
  n.getParamCached("/mission/authorized", started);

  if (started) {
           
        switch ( mode ) {
	  case MissionMode::takeoff :  // Take off 
	    {
      
	      n.setParam("/mission/mode", IDLE | GIMBALTRACK);
	      
	      takeoffPose = currOdom;
      
	      ROS_INFO("Taking off!"); 
	      drone->takeoff();  
              ros::Duration(4.5).sleep();

	      mode=MissionMode::gowatchpos;
      
              break;
           }
	  case MissionMode::gowatchpos :  // Go to watch position
           {
      
	      n.setParam("/mission/mode", GOTOWATCHPOS  | GIMBALDOWN);
      
	      //using namespace DJI::onboardSDK;
	      //uint8_t flag = Flight::HorizontalLogic::HORIZONTAL_VELOCITY | Flight::VerticalLogic::VERTICAL_VELOCITY 
	      //  | Flight::YawLogic::YAW_PALSTANCE | Flight::HorizontalCoordinate::HORIZONTAL_GROUND | Flight::YawCoordinate::YAW_GROUND | Flight::SmoothMode::SMOOTH_ENABLE;
    
	      n.getParamCached("/mission_control/zd", zd);
	      //n.getParamCached("/mission_control/kpz", kpz);
      
	      //double z_error=zd-heightSense.height(); 
      
	      // if (heightSense.height()>(0.95*zd)){
	      //    mode=MissionMode::decktrack;
	      //    n.setParam("/mission/mode", TRACK | GIMBALTRACK); // this is here to speed-up switching to track mode.
	      //    drone->attitude_control(flag, 0, 0, kpz*z_error, 0);
	      // }
	      //else{
	      //   drone->attitude_control(flag, 0, 0, kpz*z_error, 0);
	      //   mode=MissionMode::gowatchpos;      
	      //} 
      
	      // Define the final pose 
	      nav_msgs::Odometry finalPose;
	      finalPose=takeoffPose;
              //finalPose.pose.pose.position.x=0.0;
	      //finalPose.pose.pose.position.y=0.0;
	      finalPose.pose.pose.position.z=-zd;
                  
            
	      // Compute the trajectory
	      trajectory_message = computeBestTrajectory(currOdom, finalPose, 5.0, 100, nodeCount, max_speed, max_yawRate, max_pitchAngle, trajectory_message.header.seq);  
      
	      ROS_INFO("Mission: Published trajectory - Gowatchpos!");
	      trajectory_pub.publish(trajectory_message); 
        
	      ros::Duration(trajectory_message.cost+3.0).sleep();   
      
	      mode=MissionMode::decktrack;
              break;
	    }
	   case MissionMode::decktrack:  // deck tracking
            {
                                       
	      n.setParam("/mission/mode", TRACK | GIMBALTRACK);   
                
	      mode=MissionMode::decktrack;
      
	      if (heightSense.isReadyToLand() || heightSense.isLanded())
		  mode=MissionMode::land;
          
	      break;
	    }
           case MissionMode::land :  // landing
	    {
      
	      n.setParam("/mission/mode", LANDING);
      
	      ROS_INFO("Landing!");  
      
	      if (magnet.turnOn())
		ROS_INFO("Magnet is on!");  
      
	      drone->landing();     
        
              ros::Duration(5.0).sleep();   

	      if (heightSense.isLanded())
		ROS_INFO("Landed!");

              if (magnet.turnOff())
		ROS_INFO("Magnet is off!"); 

	      ros::Duration(15.0).sleep();     
        
	      mode=MissionMode::idle;
      
	      break;
	    }
	  default:
	    {
	      n.setParam("/mission/mode", IDLE);
      
	      mode=MissionMode::takeoff; 
      
	      break;
	    }
	  }
    
	} // if (started)
        else{

	    n.setParam("/mission/mode", IDLE | GIMBALTRACK); // Do nothing except for tracking the target with the Gimbal  
        }  
                 
  ros::spinOnce();
  loop_rate.sleep();
        
    }

    return 0;
}


