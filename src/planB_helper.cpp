#include "ros/ros.h"
#include <cstdlib>
#include <algorithm>
#include <ca_common/Trajectory.h>
#include <ca_common/math.h>
#include <ca_common/MissionWaypoint.h>
#include <ca_common/Mission.h>
#include <ca_common/MissionCommand.h>
#include <dji_sdk/dji_drone.h>
//#include "mbzirc_prediction/GetPrediction.h"
#include "mbzirc_polytraj/jtraj.h"
#include "mbzirc_mission/mission_modes.h"
#include "mbzirc_mission/altimeter.h"
//#include "mbzirc_mission/magnet.h"
#include "math_utils/math_utils.h"
#include <tf/transform_listener.h>
#include <opencv_apps/RotatedRectStamped.h>
#include "mbzirc_roaddetection/GetDir.h"
#include <list>
#include <termios.h>

CA::Vector3D currentXYZ;
double last_heading;
nav_msgs::Odometry currOdom;
bool gotPose = false;
ros::Time time_got_rect;
bool gotNewRectangle = false;
bool gotPositionIndex = false;
int waitfortruckposition_index = 1;
opencv_apps::RotatedRectStamped curr_rect;
int minDeckSize;
int maxDeckSize;
double ff_vx, ff_vy, onWaitForTruckPosHeight;

void getPose(const nav_msgs::OdometryConstPtr &msg)
{
    currOdom = (*msg);
    currentXYZ = CA::msgc((*msg).pose.pose.position);
    CA::QuatD orientation = CA::msgc((*msg).pose.pose.orientation);
    CA::Vector3D attitude;
    attitude = CA::quatToEuler(orientation);
    last_heading = attitude[2];
    gotPose = true;
}

void getPositionIndex(const mbzirc_roaddetection::GetDirPtr &msg)
{
  if (!gotPositionIndex){
      waitfortruckposition_index =(*msg).nextPos;
      gotPositionIndex=true;
  }
}

ca_common::Trajectory computeBestTrajectory(nav_msgs::Odometry currOdom, nav_msgs::Odometry goalOdom, double tmin, double tmax, int nodeCount, double max_speed, double max_yawRate, double max_pitchAngle, long int seq)
{

    CA::jTraj traj;
    ca_common::Trajectory trajectory_message;
    CA::Trajectory trajectory;

    double tstep=0.1;
    double t;
    for (t=tmin;t<tmax;t=t+tstep) {
        // Plan the trajectory
        traj.computeTrajectory(currOdom, goalOdom, t, nodeCount);

        // Check the feasibility of the trajectory
    if (traj.respectConstraints(max_speed, max_yawRate, max_pitchAngle)){
        ROS_INFO("Mission: Trajectory respect constraints! t=%f",t);
        break;
    }
    }

    trajectory=traj.getTrajectory();

    trajectory_message = trajectory.toMsg();
    trajectory_message.header.stamp = ros::Time::now();
    trajectory_message.header.seq=seq++;
    trajectory_message.header.frame_id = "world";
    trajectory_message.cost=t; // The cost would be the time to reach the final destination.

    return trajectory_message;

}

ca_common::Trajectory computeStraightTrajectory(nav_msgs::Odometry currOdom, nav_msgs::Odometry goalOdom, int nodeCount, long int seq)
{

    CA::jTraj traj;
    ca_common::Trajectory trajectory_message;
    CA::Trajectory trajectory;


    trajectory=traj.computeStraightLineTrajectory(currOdom, goalOdom, nodeCount);

    trajectory_message = trajectory.toMsg();
    trajectory_message.header.stamp = ros::Time::now();
    trajectory_message.header.seq=seq++;
    trajectory_message.header.frame_id = "world";
    trajectory_message.cost=(CA::math_tools::Length(CA::msgc(goalOdom.pose.pose.position)-CA::msgc(currOdom.pose.pose.position)));

    return trajectory_message;

}

enum class MissionMode : int{takeoff, gowatchpos, onwatchpos, gowaitfortruckpos,onwaitfortruckpos, movetotruck, decktrack, land, idle, approachdeck};

void ReadParams(ros::NodeHandle &n, double &loopRate, int &nodeCount, double &max_speed, double &max_speed_openLoop15, double &max_speed_openLoop10, double &max_speed_openLoop5, double &max_yawRate, double &max_pitchAngle, double &kpz, double &zd, double &ff15, double &ff10, double &ff5, double &moveDelay15, double &moveDelay10, double &moveDelay5, double &metersBelow, double &metersInFront, double &heightToApproach, bool &simulation, double &onWaitForTruckPosHeight, double &time_to_accelerate, double &giveUpTimeLaser, double &giveUpTimeLanding)
{
    n.param("/mission_control/simulation", simulation, false);

    n.param("/mission_control/loopRate", loopRate, 4.0);
    n.param("/mission_control/nodeCount", nodeCount, 20);
    n.param("/mission_control/max_speed", max_speed, 8.0);
    n.param("/mission_control/max_speed_openLoop15", max_speed_openLoop15, 6.0);
    n.param("/mission_control/max_speed_openLoop10", max_speed_openLoop10, 4.0);
    n.param("/mission_control/max_speed_openLoop5", max_speed_openLoop5, 2.0);
    n.param("/mission_control/max_yawRate", max_yawRate, 2.0);
    n.param("/mission_control/max_pitchAngle", max_pitchAngle, 0.61);

    n.param("/mission_control/kpz", kpz, 0.5);
    n.param("/mission_control/zd", zd, 2.0);

    n.param("/mission_control/ff15", ff15, 4.0);
    n.param("/mission_control/ff10", ff10, 2.0);
    n.param("/mission_control/ff5", ff5, 1.0);

    n.param("/mission_control/moveDelay15", moveDelay15, 0.5);
    n.param("/mission_control/moveDelay10", moveDelay10, 0.3);
    n.param("/mission_control/moveDelay5", moveDelay5, 0.1);

    n.param("/mission_control/giveUpTimeLaser", giveUpTimeLaser, 23.0);
    n.param("/mission_control/giveUpTimeLanding", giveUpTimeLanding, 8.5);
    
    n.param("/mission_control/minDeckSize", minDeckSize, 20);
    n.param("/mission_control/maxDeckSize", maxDeckSize, 150);
    
    n.param("/mission_control/metersInFront", metersInFront, 1.0);
    n.param("/mission_control/heightToApproach", heightToApproach, 0.5);
    n.param("/mission_control/metersBelow", metersBelow, 0.4);
    n.param("/mission_control/onWaitForTruckPosHeight", onWaitForTruckPosHeight, -5.0);
    
    n.param("/mission_control/time_to_accelerate", time_to_accelerate, 2.0);
}

void ReadPoints(ros::NodeHandle &n, double &p0x, double &p0y, double &p0z, double &p0h, double &p1x, double &p1y, double &p1z, double &p1h, double &p2x, double &p2y, double &p2z, double &p2h, double &p3x, double &p3y, double &p3z, double &p3h, double &p4x, double &p4y, double &p4z, double &p4h)
{
    n.getParam("/mission_control/p0x", p0x);
    n.getParam("/mission_control/p0y", p0y);
    n.getParam("/mission_control/p0z", p0z);
    n.getParam("/mission_control/p0h", p0h);

    n.getParam("/mission_control/p1x", p1x);
    n.getParam("/mission_control/p1y", p1y);
    n.getParam("/mission_control/p1z", p1z);
    n.getParam("/mission_control/p1h", p1h);

    n.getParam("/mission_control/p2x", p2x);
    n.getParam("/mission_control/p2y", p2y);
    n.getParam("/mission_control/p2z", p2z);
    n.getParam("/mission_control/p2h", p2h);

    n.getParam("/mission_control/p3x", p3x);
    n.getParam("/mission_control/p3y", p3y);
    n.getParam("/mission_control/p3z", p3z);
    n.getParam("/mission_control/p3h", p3h);

    n.getParam("/mission_control/p4x", p4x);
    n.getParam("/mission_control/p4y", p4y);
    n.getParam("/mission_control/p4z", p4z);
    n.getParam("/mission_control/p4h", p4h);
}

bool ReadLaserData(Altimeter &heightSense)
{
    while (!heightSense.gotData() && ros::ok())
    {
        ros::spinOnce();
        ROS_INFO("Mission - Waiting for altimeter data...");
        ros::Duration(0.5).sleep();
    }
    if (!heightSense.gotData())
    {
        ROS_ERROR("Mission - Never got altimeter data!");
        return false;
    }
    else 
        ROS_INFO("Mission - Got altimeter data...");

    return true;
}

void WaitForTransform(tf::TransformListener &listener, tf::StampedTransform &transform, CA::Vector3D &trackOrigin, double &trackOrientation)
{
    if (listener.waitForTransform("/world", "/track", ros::Time(0), ros::Duration(5.0)) ) 
    {
       ROS_INFO("Mission - Using available transformation for track");
       listener.lookupTransform("/world", "/track", ros::Time(0), transform);
       trackOrigin[0] = transform.getOrigin().x();
       trackOrigin[1] = transform.getOrigin().y();
       trackOrigin[2] = 0.0;
       trackOrientation = transform.getRotation().getAngle();
       ROS_INFO("Mission - Track Origin - X: %f Y: %f", trackOrigin[0], trackOrigin[1]);
    }
    else
    {
       ROS_WARN("Mission - No transformation received - Assuming track at (0,0)");
       trackOrigin *= 0.0;
    }
}

/**
 * @brief
 * function to detect button presses from the keyboard
 * @return int
 */
static int kbhit(void)
{
	struct termios oldt, newt;
	int ch = 0;

	tcgetattr( STDIN_FILENO, &oldt );
	newt = oldt;

	newt.c_cc[VMIN] = 0;
	newt.c_cc[VTIME] = 1;
	newt.c_lflag &= ~( ICANON | ECHO );
	tcsetattr( STDIN_FILENO, TCSANOW, &newt );

	ch = getchar();

	tcsetattr( STDIN_FILENO, TCSANOW, &oldt );

	return ch;
}

