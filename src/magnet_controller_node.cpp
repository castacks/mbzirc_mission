// Magnet control - Guilherme Pereira, Apr, 2016
// The serial port must be configured to 9600,N,8,1 with no handshaking. 
// Transmit hexadecimal A0 01 01 A2 to turn the relay on, and hexadecimal A0 01 00 A1 to turn the relay off.

#include <ros/ros.h>
#include <signal.h>
#include "mbzirc_mission/mission_modes.h"
#include "mbzirc_mission/ActuateRelay.h"
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>    /* For O_RDWR */


int fd;

void SigintHandler(int sig)
{
  write (fd, "\xA0\x01\x00\xA1", 4);           // send 7 character greeting

  usleep(4 * 100);                            // sleep enough to transmit the 4 bytes

  ros::shutdown();
}

int set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                ROS_ERROR("error %d from tcgetattr", errno);
                return -1;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                ROS_ERROR("error %d from tcsetattr", errno);
                return -1;
        }
        return 0;
}

void set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                ROS_ERROR("error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                ROS_ERROR("error %d setting term attributes", errno);
}

bool actuateRelay(mbzirc_mission::ActuateRelay::Request  &req,
         mbzirc_mission::ActuateRelay::Response &res)
{
  if (req.on){
       write (fd, "\xA0\x01\x01\xA2", 4);           // send 7 character greeting
       usleep (4 * 100);                            // sleep enough to transmit the 4 bytes
       res.on=true;
  }
  else{
       write (fd, "\xA0\x01\x00\xA1", 4);           // send 7 character greeting
       usleep(4 * 100);                             // sleep enough to transmit the 4 bytes 
       res.on=false;
  }
  return true;
}


int main(int argc, char **argv) {

  ros::init(argc, argv, "magnet_controller_node");
  ros::NodeHandle n;

  signal(SIGINT, SigintHandler);

  ros::ServiceServer service = n.advertiseService("magnet_controller/actuate", actuateRelay);
  
  std::string s;
  n.param<std::string>("magnet_controller/portname", s, "/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0");
    const char * portname = s.c_str();
  fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
  if (fd < 0)
  {
        ROS_ERROR("error %d opening %s: %s", errno, portname, strerror (errno));
        return 1;
  } 
  set_interface_attribs (fd, B9600, 0);  // set speed to 9600 bps, 8n1 (no parity)
  set_blocking (fd, 0);                  // set no blocking


  int mode = IDLE;

  // Main loop
  while (ros::ok())
     {
      
      ros::spinOnce();

      n.getParam("/mission/mode", mode);


     }
}

