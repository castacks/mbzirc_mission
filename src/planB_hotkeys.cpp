const char StartMissionKey = 'a'; // Start the mission to go to p0 point
const char RestartMissionKey = 'r'; // Restart the mission
const char StartMissionP1Key = '1'; // Start the mission to go to p1 point
const char StartMissionP2Key = '2'; // Start the mission to go to p2 point
const char StartMissionP3Key = '3'; // Start the mission to go to p3 point
const char StartMissionP4Key = '4'; // Start the mission to go to p4 point
