#include "planB_helper.cpp"
#include "planB_hotkeys.cpp"

void TakeOff(DJIDrone* &drone, bool simulation, CA::State &state, ca_common::Trajectory &trajectory_message, ros::Publisher &trajectory_pub, double &max_speed, double &max_yawRate, double &max_pitchAngle)
{
    ROS_INFO("Taking off!");

    // ****************************************************************
    // This is for the real robot
    drone->takeoff();
    // ****************************************************************

    if (simulation)
    {
        // ****************************************************************
        // This is for the simulated robot

        // Define the final pose
        state.pose.position_m=currentXYZ;
        state.pose.position_m[2] += 1.0; // Final destination is one meter above the current one.
        //state.pose.orientation_rad[2] = -M_PI/2;

        // Change the initial pose to allow take-off -> The simulator's constroller needs an abrupt movement
        currOdom.pose.pose.position.z+=0.5; // The trajectory starts above the current position

        // Compute the trajectory
        trajectory_message = computeBestTrajectory(currOdom, msgc(state), 1.0, 100, 2, max_speed, max_yawRate, max_pitchAngle, trajectory_message.header.seq);

        ROS_INFO("Mission: Published trajectory - Take off!");
        trajectory_pub.publish(trajectory_message);
        // ****************************************************************
    }
    // Wait until take off
    ros::Duration(4.5).sleep();
}

void GoWatchPos(CA::State &state, ca_common::Trajectory &trajectory_message, ros::Publisher &trajectory_pub, int &nodeCount, double &max_speed, double &max_yawRate, double &max_pitchAngle, double &p0x, double &p0y, double &p0z, double &p0h)
{
    // Define the final pose
    state.pose.position_m[0] = p0x;
    state.pose.position_m[1] = p0y;
    state.pose.position_m[2] = p0z;
    state.pose.orientation_rad[2] = p0h;

    // Compute the trajectory
    trajectory_message = computeBestTrajectory(currOdom, msgc(state), 1.0, 100, nodeCount, max_speed, max_yawRate, max_pitchAngle, trajectory_message.header.seq);

    ROS_INFO("Mission: Published %5.2f seconds trajectory - Go to watch position", (double)trajectory_message.cost);
    trajectory_pub.publish(trajectory_message);

    // Wait the time of the trajectory
    ros::Duration(trajectory_message.cost).sleep();
}

void OnWatchPos(ros::NodeHandle &n)
{
    ROS_INFO("Mission: Waiting on Watch Position");

    while (!gotPositionIndex && ros::ok())
    {
        ros::spinOnce();
        ros::Duration(0.1).sleep();
        if (kbhit() == RestartMissionKey) break;
	bool still_auto;
        n.getParamCached("/mission/authorized", still_auto);
        if (!still_auto) break;
    }

    ROS_INFO("Mission: Wait for truck in position %d", waitfortruckposition_index);
}

void GoWaitForTruckPos(CA::State &state, ca_common::Trajectory &trajectory_message, ros::Publisher &trajectory_pub, int &nodeCount, double &max_speed, double &max_yawRate, double &max_pitchAngle, double &p1x, double &p1y, double &p1z, double &p1h, double &p2x, double &p2y, double &p2z, double &p2h, double &p3x, double &p3y, double &p3z, double &p3h, double &p4x, double &p4y, double &p4z, double &p4h)
{
    // Define the final pose
    switch (waitfortruckposition_index) 
    {
      case 1:
        {
            state.pose.position_m[0]=p1x;
            state.pose.position_m[1]=p1y;
            state.pose.position_m[2]=p1z;
            state.pose.orientation_rad[2]= p1h;
            break;
        }
        case 2:
        {
            state.pose.position_m[0]=p2x;
            state.pose.position_m[1]=p2y;
            state.pose.position_m[2]=p2z;
            state.pose.orientation_rad[2]= p2h;
            break;
        }
        case 3:
        {
            state.pose.position_m[0]=p3x;
            state.pose.position_m[1]=p3y;
            state.pose.position_m[2]=p3z;
            state.pose.orientation_rad[2]= p3h;
            break;
        }
        case 4:
        {
            state.pose.position_m[0]=p4x;
            state.pose.position_m[1]=p4y;
            state.pose.position_m[2]=p4z;
            state.pose.orientation_rad[2]= p4h;
            break;
        }
    };

    // Compute the trajectory
    trajectory_message = computeBestTrajectory(currOdom, msgc(state), 1.0, 100, nodeCount, max_speed, max_yawRate, max_pitchAngle, trajectory_message.header.seq);

    ROS_INFO("Mission: Published %5.2f seconds trajectory - Go to position %d to wait for the truck", (double)trajectory_message.cost, waitfortruckposition_index);
    trajectory_pub.publish(trajectory_message);

    // Wait the time of the trajectory
    ros::Duration(trajectory_message.cost).sleep();
     
    //increase height if we are too low
//     double diffHeight=std::abs(onWaitForTruckPosHeight)-heightSense.height();
//     if (std::abs(diffHeight)>0.5){
//             state.pose.position_m[2]=state.pose.position_m[2]-diffHeight;
//             trajectory_message = computeBestTrajectory(currOdom, msgc(state), 1.0, 100, nodeCount, max_speed, max_yawRate, max_pitchAngle, trajectory_message.header.seq);
//             ROS_INFO("Mission: Published %5.2f seconds trajectory - Increased height because of GPS drift to wait for the truck", (double)trajectory_message.cost);
//             trajectory_pub.publish(trajectory_message);
//     }
//     
//     // Wait the time of the trajectory
//     ros::Duration(trajectory_message.cost).sleep();
    
}

int OnWaitForTruckPos(Altimeter &heightSense, Altimeter &laserRight, Altimeter &laserLeft, ros::NodeHandle &n, double giveUpTimeLaser)
{
    ROS_INFO("Mission: On position %d waiting for the truck", waitfortruckposition_index);

    // Trigger the quadrotor using laser data readings
    const double height_threshold = 1.2F; // Minimum drop in height to trigger
    const double side_laser_threshold = 0.8F; // Minimum drop in side laser readings to trigger
    const double time_range = 0.8F;       // Maximum time period for the drop in height
    
    ros::Time started_waiting = ros::Time::now();
    
    while ( !(heightSense.detectJump(height_threshold, time_range) == -1 || 
        laserRight.detectJump(side_laser_threshold, time_range) == -1 || 
        laserLeft.detectJump(side_laser_threshold, time_range) == -1))
    {   
        ros::spinOnce();
        if (kbhit() == RestartMissionKey) 
            break;
	 bool still_auto;
         n.getParamCached("/mission/authorized", still_auto);
         if (!still_auto) break;
	 
	ros::Duration time_wayting = ros::Time::now()-started_waiting; 
       	if (time_wayting.toSec()>giveUpTimeLaser)
	 {
	    switch (waitfortruckposition_index) 
	    {
	      case 1:
	      {
		waitfortruckposition_index = 2;
		break;
	      }
	      case 2:
	      {
		waitfortruckposition_index = 1;
		break;
	      }
	      case 3:
	      {
		waitfortruckposition_index = 4;
		break;
	      }
	      case 4:
	      {
		waitfortruckposition_index = 3;
		break;
	      }   
	   };
	   return 0;  // Did not see the truck
        }  
      ros::Duration(0.01).sleep();  
    } 
    return 1;
}


void MoveToTruck(ros::NodeHandle &n, CA::State &state, double &p1x, double &p1y, double &p1z, double &p2x, double &p2y, double &p2z, double &p3x, double &p3y, double &p3z, double &p4x, double &p4y, double &p4z, double &beginTime, double &currentTime, double &max_speed_openLoop, double &ff, double &moveDelay, double &max_speed_openLoop15, double &max_speed_openLoop5, double &ff15, double &ff5, double &moveDelay15, double &moveDelay5, CA::Vector3D &deltaPose
    ,double &max_yawRate, ca_common::Trajectory &trajectory_message, int &nodeCount, ros::Publisher &trajectory_pub, ros::Time truck_last_seen, double time_threshold_to_land)
{
    ROS_INFO("Mission: Move to truck");

    // check the current time, and choose corresponding velocities delays accordingly
    currentTime = ros::Time::now().toSec();
    ROS_WARN("curr time=%lf  beginTime=%lf ff_orig=%f",currentTime,beginTime,ff);
    n.getParamCached("/mission_control/ff15", ff15); // added to be used with code that changes FF module
//     if ((currentTime - beginTime) < 8*60)
//     {
        max_speed_openLoop=max_speed_openLoop15;
        ff=ff15;
        moveDelay=moveDelay15;
        ROS_WARN("ff15=%lf enter ff=%lf",ff15,ff);
//     } 
//     else
//     {
//         max_speed_openLoop=max_speed_openLoop5;
//         ff=ff5;
//         moveDelay=moveDelay5;
//         ROS_WARN("ff5=%f enter ff=%f",ff5,ff);
//     }
    
    ros::spinOnce();

    // Define the final pose
    switch (waitfortruckposition_index) 
    {
        case 1: // From 1 to 3
        {
            state.pose.position_m[0]=p3x;
            state.pose.position_m[1]=p3y;
            state.pose.position_m[2]=p3z;
            deltaPose[0]=state.pose.position_m[0]-p1x;
            deltaPose[1]=state.pose.position_m[1]-p1y;
            //ROS_INFO("Mission: 1->3 world frame ffX=%f ffY=%f angle=%f",ff*cos(state.pose.orientation_rad[2]), ff*sin(state.pose.orientation_rad[2]), atan2(deltaPose[1], deltaPose[0]) );
            ROS_WARN("Mission: 1->3 world frame P3x=%f P3y=%f P3z=%f P1x=%f P1y=%f P1z=%f ",p3x,p3y,p3z,p1x,p1y,p1z);
            break;
        }
        case 2: // From 2 to 4
        {
            state.pose.position_m[0]=p4x;
            state.pose.position_m[1]=p4y;
            state.pose.position_m[2]=p4z;
            deltaPose[0]=state.pose.position_m[0]-p2x;
            deltaPose[1]=state.pose.position_m[1]-p2y;
            //ROS_INFO("Mission: 2->4 %f %f %f",p2h*180/M_PI, ff*cos(p2h), ff*sin(p2h));
            break;
        }
        case 3: // From 3 to 1
        {
            state.pose.position_m[0]=p1x;
            state.pose.position_m[1]=p1y;
            state.pose.position_m[2]=p1z;
            deltaPose[0]=state.pose.position_m[0]-p3x;
            deltaPose[1]=state.pose.position_m[1]-p3y;
            //ROS_INFO("Mission: 3->1 %f %f %f",p3h*180/M_PI,  ff*cos(p3h),  ff*sin(p3h));
            break;
        }
        case 4: // From 4 to 2
        {
            state.pose.position_m[0]=p2x;
            state.pose.position_m[1]=p2y;
            state.pose.position_m[2]=p2z;
            deltaPose[0]=state.pose.position_m[0]-p4x;
            deltaPose[1]=state.pose.position_m[1]-p4y;
            //ROS_INFO("Mission: 4->2 %f %f %f",p4h*180/M_PI,  ff*cos(p4h),  ff*sin(p4h));
            break;
        }
    };
   
    state.pose.orientation_rad[2] = atan2(deltaPose[1], deltaPose[0]);
    ff_vx = ff*cos(state.pose.orientation_rad[2]);
    ff_vy = ff*sin(state.pose.orientation_rad[2]);
    n.setParam("/visual_servoing/ff_vx",  ff_vx);
    n.setParam("/visual_servoing/ff_vy",  ff_vy);
    ROS_WARN("Inside planB modes: 1->3 world frame ffX=%f ffY=%f ff=%f angle=%f",ff_vx, ff_vy, ff, state.pose.orientation_rad[2]);
    
   currOdom.twist.twist.linear.x=max_speed_openLoop*cos(state.pose.orientation_rad[2]); // set the initial speed to the maximum
   currOdom.twist.twist.linear.y=max_speed_openLoop*sin(state.pose.orientation_rad[2]);
   currOdom.twist.twist.linear.z=0.0;
   currOdom.twist.twist.angular.z=max_yawRate;

//          //if we want to double the straight line distance, uncomment lines below:
//          // state.pose.position_m[0]=state.pose.position_m[0]+0.5*deltaPose[0];
//          // state.pose.position_m[1]=state.pose.position_m[1]+0.5*deltaPose[1];

   trajectory_message = computeStraightTrajectory(currOdom, msgc(state), nodeCount, trajectory_message.header.seq);
   ROS_INFO("Mission: Published %5.2f meters trajectory - Go to truck", (double)trajectory_message.cost);

//              // let's try not publishing the trajectory in the begining
//              // UNCOMMENT BELOW TO GET BACK
   trajectory_pub.publish(trajectory_message);


//          // While it is moving, when it sees the deck -> land/home/rbonatti/mbzirc_ws/src/mbzirc_mission/src/planB.cpp
//          // UNCOMMENT BELOW TO GET BACK
   ros::Duration(moveDelay).sleep(); // delays to give time to the robot to accelerate

   // Only switct to the next node when sees the truck again 
   ros::spinOnce();
   gotNewRectangle=false;
   while (!(gotNewRectangle && (curr_rect.rect.size.width > minDeckSize && curr_rect.rect.size.width < maxDeckSize) && (curr_rect.rect.size.height > minDeckSize && curr_rect.rect.size.height < maxDeckSize)) && ros::ok()) { // Wait until it sees the deck for the fist time
       ros::spinOnce();
       ROS_INFO("Mission: Looking for the truck");
       ros::Duration(0.05).sleep();
       if ((ros::Time::now()-truck_last_seen).toSec() > time_threshold_to_land)
           break;
       
       bool still_auto;
       n.getParamCached("/mission/authorized", still_auto);
       if (!still_auto) break;
       
       if (kbhit() == RestartMissionKey) break;
   }

}

void ApproachDeck(CA::State &state, ca_common::Trajectory &trajectory_message, ros::Publisher &trajectory_pub, double &ff, double &metersInFront, double &metersBelow, double &max_yawRate, int &nodeCount)
{
    // Define the target
    state.pose.position_m[0]=currentXYZ[0]+metersInFront*cos(state.pose.orientation_rad[2]);
    state.pose.position_m[1]=currentXYZ[1]+metersInFront*sin(state.pose.orientation_rad[2]);
    state.pose.position_m[2]=currentXYZ[2]+metersBelow;

    currOdom.twist.twist.linear.x=ff*cos(state.pose.orientation_rad[2]); // set the initial speed to the maximum
    currOdom.twist.twist.linear.y=ff*sin(state.pose.orientation_rad[2]);
    currOdom.twist.twist.linear.z=ff;
    currOdom.twist.twist.angular.z=max_yawRate;

    trajectory_message = computeStraightTrajectory(currOdom, msgc(state), nodeCount, trajectory_message.header.seq);
    ROS_INFO("Mission: Published %5.2f meters trajectory - Approach the deck", (double)trajectory_message.cost);

    trajectory_pub.publish(trajectory_message);

    ros::Duration(1.0).sleep();
}

void Land(Altimeter &heightSense, DJIDrone* &drone)
{
    ROS_INFO("Mission: Landing");	  

//     if (magnet.turnOn())
//         ROS_INFO("Mission: Magnet is on!");

    drone->landing();

    ros::Duration(5.0).sleep();

    if (heightSense.isLanded())
        ROS_INFO("Mission: Landed!");

//     if (magnet.turnOff())
//         ROS_INFO("Mission: Magnet is off!");

    ros::Duration(2.0).sleep();
}
