//****************************************************************
// tf_broadcaster - Guilherme Pereira, April 2016
//		
// Dji Odometry -> X North, Y East, Z Down (NED convention)  
// world  ->  X North, Y West, Z Up 
// base_frame -> X front, Y right, Z Down (Aeronautics convention)
// gimble -> X front, Y right, Z Down
// camera -> Z orthogonal to the image pointing into the plane of the image, X points to right of the image (left of the camera), Y pointing down
//           This reference frame is defined following the standard in http://wiki.ros.org/image_pipeline/CameraInfo
// camera_visp -> Z orthogonal to the image pointing out of the image, X points to the left of the image (right of the camera), Y pointing down

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <dji_sdk/Gimbal.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <math.h>



void odomCallback(const nav_msgs::Odometry::ConstPtr &msg){
  static tf::TransformBroadcaster br;
  tf::Transform transform;

  // NED to world - This is a static tranformation. It could be done in the launch file, but I think it is more guaranteed if this is done here.
  transform.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
  tf::Quaternion q1;
  q1.setRPY(M_PI, 0, 0);
  transform.setRotation(q1);
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "ned"));
 
  // NED to base_frame 
  transform.setOrigin(tf::Vector3(msg->pose.pose.position.x, msg->pose.pose.position.y, msg->pose.pose.position.z) );
  tf::Quaternion q(msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
  transform.setRotation(q);
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "ned", "base_frame"));
}



void gimbalCallback(const dji_sdk::Gimbal::ConstPtr &msg){
  static tf::TransformBroadcaster br;
  static tf::TransformListener listener;
  tf::Transform transform;

  geometry_msgs::PointStamped origin; // origin of the gimbal in the base_frame
  origin.point.x=0.14;
  origin.point.y=0.00;
  origin.point.z=0.11;
  origin.header.frame_id="base_frame";  

  // Get the position of the robot in ned - the angle of the gimbal is not fixed to the base but the position is!
  try{
      listener.transformPoint("ned", origin, origin);
     }
  catch (tf::TransformException &ex) {
      //ROS_WARN("dji_tf_broadcaster: %s",ex.what());
      ros::Duration(2.0).sleep();
      return;
    } 
  
  // world to gimbal - dji publishes gimble orientation in relation to ned not base-frame!
  transform.setOrigin(tf::Vector3(origin.point.x, origin.point.y, origin.point.z));
  tf::Quaternion q;
  q.setRPY(msg->roll*M_PI/180.0, msg->pitch*M_PI/180.0, msg->yaw*M_PI/180.0);
  transform.setRotation(q);
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "ned", "gimbal"));

  // gimbal to camera - This is a static tranformation. It could be done in the launch file, but I think it is more guaranteed if this is done here.
  transform.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
  //q.setEuler(M_PI/2, 0, -M_PI/2); // Z points forward, X points right, Y points up
  q.setEuler(-M_PI/2, 0, -M_PI/2); // Z points backards, X points right, Y points down - CameraInfo standard
  transform.setRotation(q);
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "gimbal", "camera"));

  // gimbal to camera_visp - This is a static tranformation. It could be done in the launch file, but I think it is more guaranteed if this is done here.
  transform.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
  q.setEuler(M_PI/2, 0, M_PI/2); // Z points forward, X points left, Y points down
  transform.setRotation(q);
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "gimbal", "camera_visp"));
}



int main(int argc, char** argv){
  ros::init(argc, argv, "dji_tf_broadcaster");

  ros::NodeHandle node;
  ros::Subscriber sub1 = node.subscribe("/dji_sdk/odometry", 1, &odomCallback);
  ros::Subscriber sub2 = node.subscribe("/dji_sdk/gimbal", 1, &gimbalCallback);

  ros::spin();
  return 0;
}
