#include "ros/ros.h"
#include <cstdlib>
#include <algorithm>
#include <ca_common/Trajectory.h>
#include <ca_common/math.h>
#include <ca_common/MissionWaypoint.h>
#include <ca_common/Mission.h>
#include <ca_common/MissionCommand.h>
#include <dji_sdk/dji_drone.h>
#include "mbzirc_prediction/GetPrediction.h"
#include "mbzirc_polytraj/jtraj.h"
#include "mbzirc_mission/mission_modes.h"
#include "mbzirc_mission/altimeter.h"
#include "mbzirc_mission/magnet.h"
#include "math_utils/math_utils.h"
#include <tf/transform_listener.h>


CA::Vector3D currentXYZ;
double last_heading;
nav_msgs::Odometry currOdom;
bool gotPose = false;


void getPose(const nav_msgs::OdometryConstPtr &msg)
{
    currOdom = (*msg);
    currentXYZ = CA::msgc((*msg).pose.pose.position);
    CA::QuatD orientation = CA::msgc((*msg).pose.pose.orientation);
    CA::Vector3D attitude;
    attitude = CA::quatToEuler(orientation);
    last_heading = attitude[2];
    gotPose = true;
} 

enum class MissionMode : int{takeoff, gowatchpos, land, decktrack, idle};

ca_common::Trajectory computeBestTrajectory(nav_msgs::Odometry currOdom, nav_msgs::Odometry goalOdom, double tmin, double tmax, int nodeCount, double max_speed, double max_yawRate, double max_pitchAngle, long int seq)  
{
 
    CA::jTraj traj;
    ca_common::Trajectory trajectory_message;
    CA::Trajectory trajectory;
       
    double tstep=0.1;
    double t;
    for (t=tmin;t<tmax;t=t+tstep) { 
        // Plan the trajectory 
       	traj.computeTrajectory(currOdom, goalOdom, t, nodeCount);
                        
        // Check the feasibility of the trajectory 
 	if (traj.respectConstraints(max_speed, max_yawRate, max_pitchAngle)){
	    ROS_INFO("Trajectory respect constraints! t=%f",t);
	    break;
	}
    }

    trajectory=traj.getTrajectory();

    trajectory_message = trajectory.toMsg();
    trajectory_message.header.stamp = ros::Time::now();
    trajectory_message.header.seq=seq++;
    trajectory_message.header.frame_id = "world";
    trajectory_message.cost=t; // The cost would be the time to reach the final destination.
    
    return trajectory_message;
  
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "mission_control"); 

    ros::NodeHandle n;
    
    DJIDrone* drone;
    
    drone = new DJIDrone(n); 

    ros::Publisher trajectory_pub = n.advertise<ca_common::Trajectory>("/mission_control/trajectory", 100);
  
    double loopRate;
    int nodeCount;
    double max_speed, max_yawRate, max_pitchAngle, kpz, zd;
    bool started = false;

    n.param("/mission_control/loopRate", loopRate, 20.0);
    n.param("/mission_control/nodeCount", nodeCount, 20);
    n.param("/mission_control/max_speed", max_speed, 1.0);
    n.param("/mission_control/max_yawRate", max_yawRate, 2.0);
    n.param("/mission_control/max_pitchAngle", max_pitchAngle, 0.61);
    
    n.param("/mission_control/kpz", kpz, 0.5);
    n.param("/mission_control/zd", zd, 2.0);
    
    n.param("/mission/authorized", started, false);
    
    // Set point
    double posx, posy, posz, posh, mintime;
    
    n.param("/mission_control/posx", posx, 10.0);
    n.param("/mission_control/posy", posy, 0.0);
    n.param("/mission_control/posz", posz, 3.0);
    n.param("/mission_control/posh", posh, 0.0);
    n.param("/mission_control/mintime", mintime, 5.0);
    

    Altimeter heightSense(n);

    ros::Subscriber subLaser = n.subscribe("/guidance/ultrasonic", 1 , &Altimeter::LaserCB, &heightSense);

    while (!heightSense.gotData() && ros::ok()){
	ros::spinOnce();
        ROS_INFO("Mission - Waiting for altimeter data...");
	ros::Duration(0.5).sleep();
    }
    if (!heightSense.gotData()){
        ROS_ERROR("Mission - Never got altimeter data!");
	return false;
    }
    else ROS_INFO("Mission - Got altimeter data...");	

    ros::Subscriber subPose = n.subscribe("odom", 1, &getPose);
   
    ros::ServiceClient prediction_client = n.serviceClient<mbzirc_prediction::GetPrediction>("/prediction_node/prediction");	

    ros::Rate loop_rate(loopRate);
    
    Magnet magnet(n);

    magnet.turnOff();  

    ros::Duration(1.0).sleep(); 

    // Get transform with track position
    CA::Vector3D trackOrigin;
    double trackOrientation;
    tf::TransformListener listener;
    tf::StampedTransform transform;

    if (listener.waitForTransform("/world", "/track", ros::Time(0), ros::Duration(5.0)) ) {
       ROS_INFO("Mission - Using available transformation for track");
       listener.lookupTransform("/world", "/track", ros::Time(0), transform);
       trackOrigin[0] = transform.getOrigin().x();
       trackOrigin[1] = transform.getOrigin().y();
       trackOrigin[2] = 0.0;
       trackOrientation = transform.getRotation().getAngle();	
       ROS_INFO("Mission - Track Origin - X: %f Y: %f", trackOrigin[0], trackOrigin[1]);
    }
    else{
       ROS_WARN("Mission - No transformation received - Assuming track at (0,0)");
       trackOrigin*=0.0;
    }

    currentXYZ[0]=currentXYZ[1]=currentXYZ[2]=0;

    std::vector<CA::State, Eigen::aligned_allocator<CA::Vector3D> > state_list;
    CA::State state;	 
    CA::Trajectory trajectory;
    ca_common::Trajectory trajectory_message;
    trajectory_message.header.seq=1;

      
    ros::Duration(2).sleep();	
    
    double heading;
    CA::Vector3D deltaPose;

    n.setParam("/mission/mode", IDLE); 
    
    MissionMode mode = MissionMode::takeoff;  // Controls the state machine

    while (ros::ok()) {
	
	n.getParamCached("/mission/authorized", started);

	if (started) {
           
           switch ( mode ) {
	     case MissionMode::takeoff :  // Take off 
	        {
		  
		  n.setParam("/mission/mode", IDLE | GIMBALTRACK);
		  
		  ROS_INFO("Taking off!"); 
		  drone->takeoff();  
                  ros::Duration(4.5).sleep();

		  mode=MissionMode::gowatchpos;
		  
  	       	  break;
	       	}
	    case MissionMode::gowatchpos :  // Go to watch position
	        {
		  
		  
		  n.setParam("/mission/mode",  GOTOWATCHPOS);
		  
		  ros::spinOnce();

		  //////////// Goto some position
		  
		  // Define the final pose 
		  state.pose.position_m[0]=posx;
		  state.pose.position_m[1]=posy; 
		  state.pose.position_m[2]=posz;
		  state.pose.orientation_rad[2]=posh; 
		
		  // Compute the trajectory
		 /* trajectory_message = computeBestTrajectory(currOdom, msgc(state), mintime, 100, nodeCount, max_speed, max_yawRate, max_pitchAngle, trajectory_message.header.seq);  
	          
		  ROS_INFO("Mission: Published %5.2f seconds trajectory - Go to watch position", (double)trajectory_message.cost);
		  trajectory_pub.publish(trajectory_message);  	
	       	
		  // Wait the time of the trajectory
                  ros::Duration(trajectory_message.cost).sleep();
		  */
		 
		 
		  // Send to DJI - trajectory velocities are computed in the world reference frame
		  using namespace DJI::onboardSDK;
		  uint8_t flag = Flight::HorizontalLogic::HORIZONTAL_POSITION | Flight::VerticalLogic::VERTICAL_POSITION 
			  | Flight::YawLogic::YAW_ANGLE | Flight::HorizontalCoordinate::HORIZONTAL_GROUND | Flight::YawCoordinate::YAW_GROUND;
		  drone->attitude_control(flag, state.pose.position_m[0], state.pose.position_m[1], state.pose.position_m[2], state.pose.orientation_rad[2]*180/M_PI);
		 
		  ROS_INFO("Speed: %f",  drone->mission_waypoint_get_speed());
		  
		  ros::Duration(mintime).sleep();
		  
		  //drone->mission_waypoint_set_speed(8.0);
		  
		  // Stop the robot
		  flag = Flight::HorizontalLogic::HORIZONTAL_VELOCITY | Flight::VerticalLogic::VERTICAL_VELOCITY 
			  | Flight::YawLogic::YAW_PALSTANCE | Flight::HorizontalCoordinate::HORIZONTAL_GROUND | Flight::YawCoordinate::YAW_GROUND;
		  drone->attitude_control(flag, 0, 0, 0, 0);
		  
		  
		  ros::Duration(10).sleep();
		  
		 /* ros::spinOnce();

		  //////////// Turn in place
		  
		  // Define the final pose 
		  state.pose.position_m[0]=posx;
		  state.pose.position_m[1]=posy; 
		  state.pose.position_m[2]=posz;
		  state.pose.orientation_rad[2]=posh*M_PI/180-M_PI; 
		
		  // Compute the trajectory
		  trajectory_message = computeBestTrajectory(currOdom, msgc(state), mintime, 100, nodeCount, max_speed, max_yawRate, max_pitchAngle, trajectory_message.header.seq);  
	          
		  ROS_INFO("Mission: Published %5.2f seconds trajectory - Go to watch position", (double)trajectory_message.cost);
		  trajectory_pub.publish(trajectory_message);  	
	       	
		  // Wait the time of the trajectory
                  ros::Duration(trajectory_message.cost).sleep();
		  
		  
		  ros::Duration(5).sleep();
		  
		  ros::spinOnce();		  
		  
		  //////////// Turn in place again
		  
		  // Define the final pose 
		  state.pose.position_m[0]=posx;
		  state.pose.position_m[1]=posy; 
		  state.pose.position_m[2]=posz;
		  state.pose.orientation_rad[2]=posh*M_PI/180; 
		
		  // Compute the trajectory
		  trajectory_message = computeBestTrajectory(currOdom, msgc(state), mintime, 100, nodeCount, max_speed, max_yawRate, max_pitchAngle, trajectory_message.header.seq);  
	          
		  ROS_INFO("Mission: Published %5.2f seconds trajectory - Go to watch position", (double)trajectory_message.cost);
		  trajectory_pub.publish(trajectory_message);  	
	       	
		  // Wait the time of the trajectory
                  ros::Duration(trajectory_message.cost).sleep();
		  		  
		 
		  		  
		  //////////// Go back to the previous position
		  
		  // Define the final pose 
		  state.pose.position_m[0]=0.0;
		  state.pose.position_m[1]=0.0; 
		  state.pose.position_m[2]=posz;
		  state.pose.orientation_rad[2]=0; 
		
		  // Compute the trajectory
		  trajectory_message = computeBestTrajectory(currOdom, msgc(state), mintime, 100, nodeCount, max_speed, max_yawRate, max_pitchAngle, trajectory_message.header.seq);  
	          
		  ROS_INFO("Mission: Published %5.2f seconds trajectory - Go to watch position", (double)trajectory_message.cost);
		  trajectory_pub.publish(trajectory_message);  	
	       	
		  // Wait the time of the trajectory
                  ros::Duration(trajectory_message.cost).sleep();
		  
		  
		  ros::Duration(5).sleep();

		  ros::spinOnce();
		*/
		 
		 
		  mode=MissionMode::land;
		  
  	       	  break;
	       	}   	
           
            case MissionMode::land :  // landing
	       	{
		  
		  n.setParam("/mission/mode", LANDING);
		  
		  ROS_INFO("Landing!");  
		  
		  drone->landing();	    
  		  
                  ros::Duration(5.0).sleep(); 	

		  if (heightSense.isLanded())
		    	ROS_INFO("Landed!");

	       	  mode=MissionMode::idle;
		  
		  break;
		}
	  default:
		{
		  n.setParam("/mission/mode", IDLE);
		  
		  mode=MissionMode::idle; 
		  
		  break;
		}
	   }
	 	
	} // if (started)
        else{

 	     n.setParam("/mission/mode", IDLE); // Do nothing except for tracking the target with the Gimbal 	
        }  
                 
	ros::spinOnce();
	loop_rate.sleep();
        
    }

    return 0;
}


